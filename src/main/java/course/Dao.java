package course;

import java.util.List;

public interface Dao<T> {

    List<T> getAllFrom(CourseDao.INSTITUTION inst);
}
