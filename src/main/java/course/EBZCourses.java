package course;

import io.quarkus.logging.Log;
import rest.model.EscoSkill;
import rest.model.Topic;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;

public class EBZCourses {
    public static List<Course> getAllCourse() {
        // EBZ courses
        List<Course> coursesEbz = new ArrayList<>();
        // add ebz course
        coursesEbz.add(createCourseWith("Schaltbefähigung für das Betreiben elektrischer Anlagen bis <110 kV"));
        coursesEbz.add(createCourseWith("Schaltbefähigung für das Betreiben elektrischer Anlagen bis <120 kV"));
        coursesEbz.add(createCourseWith("Schaltbefähigung für das Betreiben elektrischer Anlagen bis 30 kV"));
        return coursesEbz;
    }

    /**
     * Create course "Schaltbefähigung für das Betreiben elektrischer Anlagen" with different titles
     *
     * @param title
     * @return
     */
    private static Course createCourseWith(String title) {
        Course courseEbz = new Course();
        courseEbz.setTitle(title);
        List<Topic> topics = new ArrayList<>();

        // 1 Befähigung und Handlungsqualifikation als schaltberechtigte Person
        Topic topic1 = new Topic();
        topic1.setTitle("Befähigung und Handlungsqualifikation als schaltberechtigte Person");
        List<String> learningOutcomes1 = new ArrayList<>();
        learningOutcomes1.add("Die TN verfügen über das notwendige spezielle Fachwissen zur Befähigung und Handlungsqualifikation als schaltberechtigte Person.");
        List<EscoSkill> escoSkills1 = new ArrayList<>();
        try {
            escoSkills1.add(new EscoSkill(new URL("http://data.europa.eu/esco/skill/c8ac1986-38fa-43b2-89dc-50a4beeb420e")));
            escoSkills1.add(new EscoSkill(new URL("http://data.europa.eu/esco/isced-f/0713")));
            escoSkills1.add(new EscoSkill(new URL("http://data.europa.eu/esco/skill/57869946-cec2-4121-981c-84c9846c58d0")));
            escoSkills1.add(new EscoSkill(new URL("http://data.europa.eu/esco/skill/56a13f47-ad7c-451f-81cf-e550107abc03")));
            escoSkills1.add(new EscoSkill(new URL("http://data.europa.eu/esco/skill/d15e92c4-c8dc-48cd-a1f6-94fe13347f87")));
            escoSkills1.add(new EscoSkill(new URL("http://data.europa.eu/esco/skill/fe96f581-4905-4282-819c-922c68bda989")));
            escoSkills1.add(new EscoSkill(new URL("http://data.europa.eu/esco/skill/2feeef46-05cf-445c-9ca1-c96a30e10106")));
            escoSkills1.add(new EscoSkill(new URL("http://data.europa.eu/esco/skill/cffc3e97-e942-4b13-a2f3-0bf4910c06d3")));
        } catch (MalformedURLException e) {
            Log.warn("Wrong ESCO URL", e);
        }
        topic1.setLearningOutcomes(learningOutcomes1);
        topic1.setEscoSkills(escoSkills1);
        topics.add(topic1);

        // 2 Allgemeine rechtskonforme Grundlagen und Neuerungen
        Topic topic2 = new Topic();
        topic2.setTitle("Allgemeine rechtskonforme Grundlagen und Neuerungen");
        List<String> learningOutcomes2 = new ArrayList<>();
        learningOutcomes2.add("Die TN können Schalthandlungen und Arbeiten an elektrischen Anlagen, deren Spannungshöhe nach der Norm DIN VDE 0100 1.000 V überschreitet und 110kV unterschreitet sicher und fachgerecht gemäß den VDE- und UVV-Bestimmungen durchführen.");
        List<EscoSkill> escoSkills2 = new ArrayList<>();
        try {
            escoSkills2.add(new EscoSkill(new URL("http://data.europa.eu/esco/skill/dd6e5939-d19b-4e2f-a025-2014f0a2ec88")));
            escoSkills2.add(new EscoSkill(new URL("http://data.europa.eu/esco/skill/1e38f5be-bb7d-42c5-8301-c16cf400f8ee")));
            escoSkills2.add(new EscoSkill(new URL("http://data.europa.eu/esco/skill/2bbdea06-a265-42b5-bb4f-7241b661f069")));
            escoSkills2.add(new EscoSkill(new URL("http://data.europa.eu/esco/skill/7263d760-f114-42b8-8d60-3b9e8107e990")));
        } catch (MalformedURLException e) {
            Log.warn("Wrong ESCO URL", e);
        }
        topic2.setLearningOutcomes(learningOutcomes2);
        topic2.setEscoSkills(escoSkills2);
        topics.add(topic2);

        // 3 Betreiben elektrischer Anlagen (DGUV Vorschrift 3, DIN VDE 0105-100)
        Topic topic3 = new Topic();
        topic3.setTitle("Betreiben elektrischer Anlagen (DGUV Vorschrift 3, DIN VDE 0105-100)");
        List<String> learningOutcomes3 = new ArrayList<>();
        learningOutcomes3.add("Die TN kennen das Anforderungsprofil für die schaltberechtigte Person und die relevanten VDE- und DGUV-Bestimmungen zum Betrieb elektrischer Anlagen bis <110kV.");
        List<EscoSkill> escoSkills3 = new ArrayList<>();
        try {
            escoSkills3.add(new EscoSkill(new URL("http://data.europa.eu/esco/skill/dcc96a49-0bac-4a9c-8f8e-a32b4e47e6eb")));
        } catch (MalformedURLException e) {
            Log.warn("Wrong ESCO URL", e);
        }
        topic3.setLearningOutcomes(learningOutcomes3);
        topic3.setEscoSkills(escoSkills3);
        topics.add(topic3);

        // 4 Sicherheitsbestimmungen für elektrische Anlagen über 1 kV
        Topic topic4 = new Topic();
        topic4.setTitle("Sicherheitsbestimmungen für elektrische Anlagen über 1 kV");
        List<String> learningOutcomes4 = new ArrayList<>();
        learningOutcomes4.add("Die TN sind aktuell zu den  besonderen Gefahren bei elektrischen Anlagen über 1 kV unterwiesen und können Arbeiten in einen Sicherheit gewährleistenden Organisationsablauf durchführen.");
        List<EscoSkill> escoSkills4 = new ArrayList<>();
        try {
            escoSkills4.add(new EscoSkill(new URL("http://data.europa.eu/esco/skill/6360d655-3494-45f6-bfc6-d5f63afc355b")));
            escoSkills4.add(new EscoSkill(new URL("http://data.europa.eu/esco/skill/76526bf6-bada-4ca1-8e79-6b9d4228b607")));
            escoSkills4.add(new EscoSkill(new URL("http://data.europa.eu/esco/skill/6122d586-5978-431f-8e7a-96e61fc1f3fc")));
        } catch (MalformedURLException e) {
            Log.warn("Wrong ESCO URL", e);
        }
        topic4.setLearningOutcomes(learningOutcomes4);
        topic4.setEscoSkills(escoSkills4);
        topics.add(topic4);

        // 5 Arbeitsmethoden, Sicherheitsregeln und technische Voraussetzungen
        Topic topic5 = new Topic();
        topic5.setTitle("Arbeitsmethoden, Sicherheitsregeln und technische Voraussetzungen");
        List<String> learningOutcomes5 = new ArrayList<>();
        learningOutcomes5.add("Die TN kennen die Arbeitsmethoden für Arbeiten an und in der Nähe von Hochspannungsanlagen, die Regel 0 und die „5 Sicherheitsregeln“ sowie die technischen Voraussetzungen und Anforderungen an Schaltanlagen, Schalträumen und Einrichtungen. ");
        List<EscoSkill> escoSkills5 = new ArrayList<>();
        topic5.setLearningOutcomes(learningOutcomes5);
        topic5.setEscoSkills(escoSkills5);
        topics.add(topic5);

        // 6 Elektrische Bauteile zusammenfügen
        Topic topic6 = new Topic();
        topic6.setTitle("Elektrische Bauteile zusammenfügen");
        List<String> learningOutcomes6 = new ArrayList<>();
        learningOutcomes6.add("Die TN kennen Betriebs- und Arbeitsmittel, Spannungsprüfer über 1 kV und Sicherungszange für HH-Sicherungen und können diese fachgerecht anwenden.");
        List<EscoSkill> escoSkills6 = new ArrayList<>();
        try {
            escoSkills6.add(new EscoSkill(new URL("http://data.europa.eu/esco/skill/415cb0c2-e169-4f99-ba3e-d1c433b75368")));
        } catch (MalformedURLException e) {
            Log.warn("Wrong ESCO URL", e);
        }
        topic6.setLearningOutcomes(learningOutcomes6);
        topic6.setEscoSkills(escoSkills6);
        topics.add(topic6);

        // 7 Netzaufbau, besondere Gefahren und Sensibilisierung für Gefahrenpotentiale
        Topic topic7 = new Topic();
        topic7.setTitle("Netzaufbau, besondere Gefahren und Sensibilisierung für Gefahrenpotentiale");
        List<String> learningOutcomes7 = new ArrayList<>();
        learningOutcomes7.add("Die TN verfügen über anwendungsbereite Kenntnisse zum Netzaufbau im Hochspannungsbereich und zu besonderen Gefahren und deren Abwendung (z. B. durch Sicherheitsabstände und Tragen von PSAgS). Sie sind für Gefahrenpotenziale sensibilisiert.");
        List<EscoSkill> escoSkills7 = new ArrayList<>();
        try {
            escoSkills7.add(new EscoSkill(new URL("http://data.europa.eu/esco/skill/e7b05688-c191-4c34-b8ba-9cdcb8d805ad")));
            escoSkills7.add(new EscoSkill(new URL("http://data.europa.eu/esco/skill/694e1e70-3e22-4d72-a3ac-b5f92951423d")));
        } catch (MalformedURLException e) {
            Log.warn("Wrong ESCO URL", e);
        }
        topic7.setLearningOutcomes(learningOutcomes7);
        topic7.setEscoSkills(escoSkills7);
        topics.add(topic7);

        // 8 Spezialkenntnisse zu Transformatoren für die Mittelspannung, Schaltgruppe, Kurzschlussspannung, Übersetzungsverhältnis und Einrichtungen zur Unfallverhütung bei Schalthandlungen
        Topic topic8 = new Topic();
        topic8.setTitle("Spezialkenntnisse zu Transformatoren für die Mittelspannung, Schaltgruppe, Kurzschlussspannung, Übersetzungsverhältnis und Einrichtungen zur Unfallverhütung bei Schalthandlungen");
        List<String> learningOutcomes8 = new ArrayList<>();
        learningOutcomes8.add("Die TN verfügen über Spezialkenntnisse zu Transformatoren für die Mittelspannung, Schaltgruppe, Kurzschlussspannung, Übersetzungsverhältnis und Einrichtungen zur Unfallverhütung bei Schalthandlungen.");
        List<EscoSkill> escoSkills8 = new ArrayList<>();
        try {
            escoSkills8.add(new EscoSkill(new URL("http://data.europa.eu/esco/skill/fc72d86f-7e69-4bf8-b407-23303a798a0b")));
        } catch (MalformedURLException e) {
            Log.warn("Wrong ESCO URL", e);
        }
        topic8.setLearningOutcomes(learningOutcomes8);
        topic8.setEscoSkills(escoSkills8);
        topics.add(topic8);

        // 9 Rechtskonformer Organisationsablauf und Freigabeverfahren
        Topic topic9 = new Topic();
        topic9.setTitle("Rechtskonformer Organisationsablauf und Freigabeverfahren");
        List<String> learningOutcomes9 = new ArrayList<>();
        learningOutcomes9.add("Die TN können Schalthandlungen und Arbeiten fachgerecht mittels Freigabeverfahren, Schaltgenehmigung, Schaltprogramm, Schaltgespräch und Schaltauftrag kommunizieren, analysieren und dokumentieren.");
        List<EscoSkill> escoSkills9 = new ArrayList<>();
        topic9.setLearningOutcomes(learningOutcomes9);
        topic9.setEscoSkills(escoSkills9);
        topics.add(topic9);

        // 10 Betriebsmittel: Schaltgeräte, Transformatoren, Leitungen
        Topic topic10 = new Topic();
        topic10.setTitle("Betriebsmittel: Schaltgeräte, Transformatoren, Leitungen");
        List<String> learningOutcomes10 = new ArrayList<>();
        learningOutcomes10.add("Die TN kennen Betriebsmittel wie Schaltgeräte, Transformatoren, gas- und luftisolierte Anlagen – Schalt- und Steuergeräte, Fehlerarten und Schutztechnik.");
        List<EscoSkill> escoSkills10 = new ArrayList<>();
        try {
            escoSkills10.add(new EscoSkill(new URL("http://data.europa.eu/esco/skill/S8.4.4")));
        } catch (MalformedURLException e) {
            Log.warn("Wrong ESCO URL", e);
        }
        topic10.setLearningOutcomes(learningOutcomes10);
        topic10.setEscoSkills(escoSkills10);
        topics.add(topic10);

        // 11 Ursachen die zu einer Fehlhandlung führen können, Verhalten bei Störungen und Vermeidung von Fehlschaltungen
        Topic topic11 = new Topic();
        topic11.setTitle("Ursachen die zu einer Fehlhandlung führen können, Verhalten bei Störungen und Vermeidung von Fehlschaltungen");
        List<String> learningOutcomes11 = new ArrayList<>();
        learningOutcomes11.add("Die TN kennen Ursachen, die zu Fehlhandlungen führen können, wissen wie bei Störungen zu verfahren ist und wie Fehlschaltungen vermieden werden können.");
        List<EscoSkill> escoSkills11 = new ArrayList<>();
        try {
            escoSkills11.add(new EscoSkill(new URL("http://data.europa.eu/esco/skill/2b76cbbf-eddc-4a12-a120-f4ea370820a5")));
            escoSkills11.add(new EscoSkill(new URL("http://data.europa.eu/esco/skill/6542601c-92ee-4a03-abd9-fde0635727a2")));
        } catch (MalformedURLException e) {
            Log.warn("Wrong ESCO URL", e);
        }
        topic11.setLearningOutcomes(learningOutcomes11);
        topic11.setEscoSkills(escoSkills11);
        topics.add(topic11);

        courseEbz.setTopics(topics);
        // visual representation
        try {
            ClassLoader classloader = Thread.currentThread().getContextClassLoader();
            InputStream is = classloader.getResourceAsStream("course/ebz/urn:myedulife:training:1:visual_template.svg");
            String data = readFromInputStream(is);
            courseEbz.setVisualRepresentationTemplate(data);
        } catch (Exception e) {
            Log.warn("Cannot open file.", e);
        }
        return courseEbz;
    }

    private static String readFromInputStream(InputStream inputStream) throws IOException {
        StringBuilder resultStringBuilder = new StringBuilder();
        try (BufferedReader br = new BufferedReader(new InputStreamReader(inputStream))) {
            String line;
            while ((line = br.readLine()) != null) {
                resultStringBuilder.append(line).append("\n");
            }
        }
        return resultStringBuilder.toString();
    }
}
