package course;

import io.quarkus.logging.Log;
import rest.model.EscoSkill;
import rest.model.Topic;

import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

public class CourseDao implements Dao<Course> {

    public enum INSTITUTION { KOMPASS, EBZ, HWK }

    final private Map<INSTITUTION, List<Course>> courseMap = new ConcurrentHashMap<>();

    public CourseDao() {
        courseMap.put(INSTITUTION.HWK, HWKCourses.getAllCourse());
        courseMap.put(INSTITUTION.EBZ, EBZCourses.getAllCourse());
    }

    @Override
    public List<Course> getAllFrom(INSTITUTION inst) {
        return courseMap.get(inst);
    }
}
