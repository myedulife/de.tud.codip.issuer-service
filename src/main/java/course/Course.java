package course;

import lombok.Getter;
import lombok.Setter;
import rest.model.EscoSkill;
import rest.model.Topic;

import java.util.ArrayList;
import java.util.List;

@Getter
@Setter
public class Course {
    private String id;
    private String title;
    private String description;
    private String mark;
    private String markSystemId;
    private int hours;
    private String visualRepresentationTemplate;
    private List<Topic> topics = new ArrayList<>();
}
