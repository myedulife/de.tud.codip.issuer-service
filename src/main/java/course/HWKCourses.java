package course;

import io.quarkus.logging.Log;
import rest.model.EscoSkill;
import rest.model.Topic;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

public class HWKCourses {
    public static List<Course> getAllCourse() {
        // HWK courses
        List<Course> coursesHwk = new ArrayList<>();
        Course courseHwkWelding = new Course();
        courseHwkWelding.setTitle("International Welding Specialist");
        List<Topic> topics = new ArrayList<>();
        // 1 Grundlagen der schweißtechnischen Messkunde
        Topic topic1 = new Topic();
        topic1.setTitle("Grundlagen der schweißtechnischen Messkunde");
        List<String> learningOutcomes1 = new ArrayList<>();
        learningOutcomes1.add("Die Zielstellungen der Messkunde nachvollziehen können.");
        learningOutcomes1.add("Alle SI-Grundeinheiten und ihre Symbole für Länge, Masse, Zeit, elektrischen Strom, Spannung, Temperatur, ebene Winkel und andere in Bezug auf das Schweißen allgemein verwendete Einheiten aufzählen können.");
        learningOutcomes1.add("Alle SI-Grundeinheiten und ihre Symbole für Fläche, Dichte, Energie, Kraft, Frequenz, Leistung, Druck, Volumen, lineare Geschwindigkeit und andere in Bezug auf das Schweißen allgemein verwendete Einheiten aufzählen können.");
        learningOutcomes1.add("Allgemein verwendete Multiplikationsfaktoren, Vorzeichen und ihre Symbole aufzählen können.");
        learningOutcomes1.add("Die wichtigsten Geräte zur linearen Messung kennen.");
        List<EscoSkill> escoSkills1 = new ArrayList<>();
        try {
            escoSkills1.add(new EscoSkill(new URL("http://data.europa.eu/esco/skill/a181e89d-44ae-40e7-8d37-177071237c74")));
            escoSkills1.add(new EscoSkill(new URL("http://data.europa.eu/esco/skill/4339176e-3acd-4f7f-a5d9-445bee3d23f2")));
        } catch (MalformedURLException e) {
            Log.warn("Wrong ESCO URL", e);
        }
        topic1.setLearningOutcomes(learningOutcomes1);
        topic1.setEscoSkills(escoSkills1);
        topics.add(topic1);
        // 2 Rechnisches Rechnen
        Topic topic2 = new Topic();
        topic2.setTitle("Rechnisches Rechnen");
        List<String> learningOutcomes2 = new ArrayList<>();
        List<EscoSkill> escoSkills2 = new ArrayList<>();
        learningOutcomes2.add("Rechenaufgaben der Addition, Subtraktion, Multiplikation und Division von ganzen Zahlen unterschiedlicher Größe sowie Dezimalzahlen und Brüchen anhand von Beispielen veranschaulichen können.");
        learningOutcomes2.add("Exponentialrechnung mit Exponenten von 0, 1, 2 bis einschließlich 10 anhand von Beispielen veranschaulichen können.");
        learningOutcomes2.add("Die Ziehung von Quadratwurzeln von Zahlen größer und kleiner als 1 anhand von Beispielen veranschaulichen können.");
        learningOutcomes2.add("Lineare Gleichungen sowie ihre Umstellung, Bearbeitung und Lösung anhand von Beispielen veranschaulichen können.");
        learningOutcomes2.add("Grundlegende trigonometrische Funktionen von Sinus, Kosinus und Tangens anhand der Seitenverhältnisse im rechtwinkligen Dreieck erklären können.");
        learningOutcomes2.add("Länge, Fläche, Volumen, Geschwindigkeit und Beschleunigung berechnen können.");
        learningOutcomes2.add("Die Umrechnungstabellen “Metrisches versus traditionelles britisches Maßeinheitensystem” in Bezug auf Länge, Geschwindigkeit und Gasdurchflussmenge anwenden können.");
        learningOutcomes2.add("Die Umrechnungstabellen “Temperaturwerte in Kelvin, Grad Celsius und Fahrenheit” anwenden können.");
        learningOutcomes2.add("Berechnungen mit schweißtechnischem Bezug einschließlich verschiedener kombinierter Rechenabläufe, Exponenten, Quadratwurzeln, grundlegender trigonometrischer Funktionen und Gleichungen veranschaulichen können.");
        learningOutcomes2.add("Den Einsatz von Taschenrechnern für oben genannte Berechnungen und Funktionen vorführen können.");
        try {
            escoSkills2.add(new EscoSkill(new URL("http://data.europa.eu/esco/skill/4339176e-3acd-4f7f-a5d9-445bee3d23f2")));
        } catch (MalformedURLException e) {
            Log.warn("Wrong ESCO URL", e);
        }
        topic2.setLearningOutcomes(learningOutcomes2);
        topic2.setEscoSkills(escoSkills2);
        topics.add(topic2);
        // 3 Technisches Zeichnen
        Topic topic3 = new Topic();
        topic3.setTitle("Technisches Zeichnen");
        List<String> learningOutcomes3 = new ArrayList<>();
        List<EscoSkill> escoSkills3 = new ArrayList<>();
        learningOutcomes3.add("Den Zweck und die Bedeutung des technischen Zeichnens für Schweißanwendungen erklären können.");
        learningOutcomes3.add("Eine technische Zeichnung erstellen und präsentieren können.");
        learningOutcomes3.add("Projektionsansichten kennen/beherrschen.");
        learningOutcomes3.add("Den Einsatz und die Angabe von Maßstäben in Konstruktionszeichnungen erläutern können.");
        learningOutcomes3.add("Die verschiedenen Linienarten und ihre Anwendung erläutern können.");
        learningOutcomes3.add("Schnittdarstellungssymbole und -methoden erklären und unterschiedliche Schnittansichten darstellen können.");
        learningOutcomes3.add("Verschiedene Arten von Diagrammen zeichnen können.");
        try {
            escoSkills3.add(new EscoSkill(new URL("http://data.europa.eu/esco/skill/59ea80e1-463a-4dba-82c6-d0b6d577d532")));
            escoSkills3.add(new EscoSkill(new URL("http://data.europa.eu/esco/skill/541561bc-510c-4a99-881c-2d8bf5a85462")));
            escoSkills3.add(new EscoSkill(new URL("http://data.europa.eu/esco/skill/dfce2aba-783e-4e17-9c17-e74aca505ec2")));
            escoSkills3.add(new EscoSkill(new URL("http://data.europa.eu/esco/skill/3c333552-8d89-43f5-90bd-856e15bec433")));
        } catch (MalformedURLException e) {
            Log.warn("Wrong ESCO URL", e);
        }
        topic3.setLearningOutcomes(learningOutcomes3);
        topic3.setEscoSkills(escoSkills3);
        topics.add(topic3);
        // 4 Grundlagen der Elektrotechnik
        Topic topic4 = new Topic();
        topic4.setTitle("Grundlagen der Elektrotechnik");
        List<String> learningOutcomes4 = new ArrayList<>();
        List<EscoSkill> escoSkills4 = new ArrayList<>();
        learningOutcomes4.add("Die unterschiedlichen Stromquellen kurz beschreiben können.");
        learningOutcomes4.add("Die wesentlichen Unterschiede zwischen Gleich- und Wechselstrom beschreiben und Beispiele für ihre je individuellen Anwendungen nennen können.");
        learningOutcomes4.add("Den 50(60)-Hz-Wechselstrom und seine sinusförmige Wellenform beschreiben können.");
        learningOutcomes4.add("Einphasige und dreiphasige Wechselstromleitungen beschreiben können.");
        learningOutcomes4.add("Scheitelwerte, Mittelwerte und Effektivwerte für Wechselstrom und Wechselspannung bestimmen können.");
        learningOutcomes4.add("Einen Stern-Anschluss (Y) beschreiben können.");
        learningOutcomes4.add("Einen Delta-Anschluss (∆) beschreiben können.");
        learningOutcomes4.add("Die Funktionsweise von Voltmetern, Amperemetern, Ohmmetern und Mehrfachmessgeräten  einschließlich digitaler Multimeter sowie deren schweißtechnische Anwendung erläutern können.");
        learningOutcomes4.add("Schaltpläne und einfache Stromkreise lesen können.");
        try {
            escoSkills4.add(new EscoSkill(new URL("http://data.europa.eu/esco/skill/3e40c7d0-0e36-4b33-bc33-0aa87eda0561")));
            escoSkills4.add(new EscoSkill(new URL("http://data.europa.eu/esco/skill/57869946-cec2-4121-981c-84c9846c58d0")));
            escoSkills4.add(new EscoSkill(new URL("http://data.europa.eu/esco/skill/722be5b6-9492-46da-aeef-ddd880fdceb1")));
            escoSkills4.add(new EscoSkill(new URL("http://data.europa.eu/esco/skill/fe96f581-4905-4282-819c-922c68bda989")));
        } catch (MalformedURLException e) {
            Log.warn("Wrong ESCO URL", e);
        }
        topic4.setLearningOutcomes(learningOutcomes4);
        topic4.setEscoSkills(escoSkills4);
        topics.add(topic4);
        // 5 Grundlagen der Chemie
        Topic topic5 = new Topic();
        topic5.setTitle("Grundlagen der Chemie");
        List<String> learningOutcomes5 = new ArrayList<>();
        List<EscoSkill> escoSkills5 = new ArrayList<>();
        learningOutcomes5.add("Die wesentlichen chemischen Elemente und ihre Symbole im Zusammenhang mit der Anwendung von Maschinenbaustahl, Aluminium, Nickel, Kupfer und ihren Legierungen auflisten können.");
        learningOutcomes5.add("Chemische Reaktionen und ihre Beschreibung durch Gleichungen anhand von Beispielen aus der Stahlerzeugung erläutern können.");
        learningOutcomes5.add("Die chemische Zusammensetzung von Gasen (nach Volumen) und Feststoffen (nach Masse) kurz darstellen können.");
        learningOutcomes5.add("Die verschiedenen Arten von un-, niedrig- und hochlegierten Stählen sowie ihre chemische Zusammensetzung umreißen können.");
        try {
            escoSkills5.add(new EscoSkill(new URL("http://data.europa.eu/esco/skill/cde9911c-f92e-4de1-811f-336e177ac4ce")));
        } catch (MalformedURLException e) {
            Log.warn("Wrong ESCO URL", e);
        }
        topic5.setLearningOutcomes(learningOutcomes5);
        topic5.setEscoSkills(escoSkills5);
        topics.add(topic5);

        // 6 Grundlagen der Werkstoffe
        Topic topic6 = new Topic();
        topic6.setTitle("Grundlagen der Werkstoffe");
        List<String> learningOutcomes6 = new ArrayList<>();
        List<EscoSkill> escoSkills6 = new ArrayList<>();
        learningOutcomes6.add("Beschreibung, Anwendung, Arten und physikalische Eigenschaften der wichtigsten metallischen Werkstoffe.");
        learningOutcomes6.add("Die Unterschiede in den wesentlichen Eigenschaften von Stahl, Gusseisen, Aluminium und Kupfer kennen.");
        learningOutcomes6.add("Die für unterschiedliche Anwendungen jeweils geeigneten Stahlsorten auflisten können.");
        try {
            escoSkills6.add(new EscoSkill(new URL("http://data.europa.eu/esco/skill/142f3f7f-f15f-412e-a5fe-f75755b5dbe0")));
            escoSkills6.add(new EscoSkill(new URL("http://data.europa.eu/esco/skill/d1041406-2c24-4177-9b64-406621a8865e")));
        } catch (MalformedURLException e) {
            Log.warn("Wrong ESCO URL", e);
        }
        topic6.setLearningOutcomes(learningOutcomes6);
        topic6.setEscoSkills(escoSkills6);
        topics.add(topic6);

        // 7 Metallerzeugnisse
        Topic topic7 = new Topic();
        topic7.setTitle("Metallerzeugnisse");
        List<String> learningOutcomes7 = new ArrayList<>();
        List<EscoSkill> escoSkills7 = new ArrayList<>();
        learningOutcomes7.add("Die wesentlichen Arten von Schmiedeerzeugnissen aufzählen können.");
        learningOutcomes7.add("Die unterschiedlichen Schmiedeerzeugnisse und ihre korrekte Bezeichnung kennen.");
        learningOutcomes7.add("Erklären können, wie verschiedene Eigenschaften von den schmiedetechnischen Herstellungsmethoden abhängen.");
        learningOutcomes7.add("Werkstoffe anhand ihrer Bezeichnung erkennen können.");
        try {
            escoSkills7.add(new EscoSkill(new URL("http://data.europa.eu/esco/skill/7f107a85-d6ac-410f-a995-d635c5aa418b")));
        } catch (MalformedURLException e) {
            Log.warn("Wrong ESCO URL", e);
        }
        topic7.setLearningOutcomes(learningOutcomes7);
        topic7.setEscoSkills(escoSkills7);
        topics.add(topic7);

        // 8 Werkstoffbearbeitung
        Topic topic8 = new Topic();
        topic8.setTitle("Werkstoffbearbeitung");
        List<String> learningOutcomes8 = new ArrayList<>();
        List<EscoSkill> escoSkills8 = new ArrayList<>();
        learningOutcomes8.add("Die wichtigsten Bearbeitungsmethoden kennen.");
        learningOutcomes8.add("Den Unterschied zwischen Schneid- und Schleifbearbeitung beschreiben können.");
        learningOutcomes8.add("Die wesentlichen Schleifmethoden aufzählen können.");
        learningOutcomes8.add("Die wichtigsten Schneidmethoden aufzählen können.");
        learningOutcomes8.add("Schmirgel-, Schneid- oder Abdrehräder in Abhängigkeit vom Werkstoff und der Form des Werkstückes auswählen können.");
        try {
            escoSkills8.add(new EscoSkill(new URL("http://data.europa.eu/esco/skill/337e06b0-29e3-47bf-95a3-19bd84b311de")));
        } catch (MalformedURLException e) {
            Log.warn("Wrong ESCO URL", e);
        }
        topic8.setLearningOutcomes(learningOutcomes8);
        topic8.setEscoSkills(escoSkills8);
        topics.add(topic8);

        // 9 Technische Mechanik
        Topic topic9 = new Topic();
        topic9.setTitle("Technische Mechanik");
        List<String> learningOutcomes9 = new ArrayList<>();
        List<EscoSkill> escoSkills9 = new ArrayList<>();
        learningOutcomes9.add("Kräfte graphisch zerlegen können.");
        learningOutcomes9.add("Die resultierende Kraft mehrerer Kräfte durch einen Punkt bestimmen können.");
        learningOutcomes9.add("Einfache Biegemomente und Biegekräfte bestimmen können.");
        learningOutcomes9.add("Stützkräfte (Reaktionen) berechnen können.");
        try {
            escoSkills9.add(new EscoSkill(new URL("http://data.europa.eu/esco/skill/efa141df-f382-418f-9121-bd88fd735669")));
        } catch (MalformedURLException e) {
            Log.warn("Wrong ESCO URL", e);
        }
        topic9.setLearningOutcomes(learningOutcomes9);
        topic9.setEscoSkills(escoSkills9);
        topics.add(topic9);

        // 10 Verbindungselemente
        Topic topic10 = new Topic();
        topic10.setTitle("Verbindungselemente");
        List<String> learningOutcomes10 = new ArrayList<>();
        List<EscoSkill> escoSkills10 = new ArrayList<>();
        learningOutcomes10.add("Die unterschiedlichen Arten der Werkstoffverbindung aufzählen können.");
        learningOutcomes10.add("Lösbare und unlösbare (geschweißte) Verbindungen vergleichen können.");
        learningOutcomes10.add("Den Unterschied zwischen statischen Verbindungen und Übertragungsverbindungen kennen.");
        try {
            escoSkills10.add(new EscoSkill(new URL("http://data.europa.eu/esco/skill/5b0a8b4d-7c71-43b1-8e52-4c0cfacd7bbd")));
            escoSkills10.add(new EscoSkill(new URL("http://data.europa.eu/esco/skill/306ca6bc-bacd-434e-91d9-249a518ec71e")));
        } catch (MalformedURLException e) {
            Log.warn("Wrong ESCO URL", e);
        }
        topic10.setLearningOutcomes(learningOutcomes10);
        topic10.setEscoSkills(escoSkills10);
        topics.add(topic10);

        // 11 Festigkeitslehre
        Topic topic11 = new Topic();
        topic11.setTitle("Festigkeitslehre");
        List<String> learningOutcomes11 = new ArrayList<>();
        List<EscoSkill> escoSkills11 = new ArrayList<>();
        learningOutcomes11.add("Ein Spannungs-Dehnungs-Schaubild zeichnen können.");
        learningOutcomes11.add("Ein Spannungs-Dehnungs-Schaubild erklären können.");
        learningOutcomes11.add("Spannungen berechnen/verifizieren können.");
        learningOutcomes11.add("Ein Widerstandsmoment berechnen können.");
        learningOutcomes11.add("Ein Trägheitsmoment berechnen können.");
        learningOutcomes11.add("Querschnitte bestimmen können.");
        try {
            escoSkills11.add(new EscoSkill(new URL("http://data.europa.eu/esco/skill/82b6b024-6369-4d84-ba68-c665b9df53f8")));
        } catch (MalformedURLException e) {
            Log.warn("Wrong ESCO URL", e);
        }
        topic11.setLearningOutcomes(learningOutcomes11);
        topic11.setEscoSkills(escoSkills11);
        topics.add(topic11);

        // 12 Allgemeine Einführung in die Schweißtechnik
        Topic topic12 = new Topic();
        topic12.setTitle("Allgemeine Einführung in die Schweißtechnik");
        List<String> learningOutcomes12 = new ArrayList<>();
        List<EscoSkill> escoSkills12 = new ArrayList<>();
        learningOutcomes12.add("Die wesentlichen Unterschiede zwischen den einzelnen Schweißprozessen, z.B. Schmelz-, Widerstands, Flamm- und Pressschweißen usw. umreißen können.");
        learningOutcomes12.add("Die unterschiedlichen Prozesse unter Bezugnahme auf bestehende Normen beschreiben können.");
        learningOutcomes12.add("Einen Schweißprozess anhand der üblichen Abkürzung erkennen können.");
        try {
            escoSkills12.add(new EscoSkill(new URL("http://data.europa.eu/esco/skill/0bb4b474-da3a-45ce-9de3-0c964d84e73f")));
            escoSkills12.add(new EscoSkill(new URL("http://data.europa.eu/esco/skill/530cb489-d900-46ab-9350-4d50ccd49eea")));
            escoSkills12.add(new EscoSkill(new URL("http://data.europa.eu/esco/skill/29d762da-1711-4a3c-b424-f0a404f4bf2d")));
            escoSkills12.add(new EscoSkill(new URL("http://data.europa.eu/esco/skill/8ed0f653-795d-422b-8856-a55ec250bb88")));
        } catch (MalformedURLException e) {
            Log.warn("Wrong ESCO URL", e);
        }
        topic12.setLearningOutcomes(learningOutcomes12);
        topic12.setEscoSkills(escoSkills12);
        topics.add(topic12);

        // 13 Autogenschweißen und verwandte Verfahren
        Topic topic13 = new Topic();
        topic13.setTitle("Autogenschweißen und verwandte Verfahren");
        List<String> learningOutcomes13 = new ArrayList<>();
        List<EscoSkill> escoSkills13 = new ArrayList<>();
        learningOutcomes13.add("Die Eigenschaften der drei Flammarten und die für sie typischen Anwendungen umreißen können.");
        learningOutcomes13.add("Die Flammeigenschaften der unterschiedlichen Brenngase erkennen können.");
        learningOutcomes13.add("Die möglichen Gefahren und Maßnahmen zum sicheren Umgang, Abstellen/Lagern und Arbeiten erkennen können.");
        learningOutcomes13.add("Zweck und Arbeitsweise der einzelnen Ausrüstungsteile kurz darstellen können.");
        learningOutcomes13.add("Kenntnisse über die Anwendung der einschlägigen Normen nachweisen können.");
        learningOutcomes13.add("Die prozessspezifischen Einschränkungen und Anwendungsbereiche erkennen können und verstehen, wie prozessspezifische Probleme zu lösen sind.");
        try {
            escoSkills13.add(new EscoSkill(new URL("http://data.europa.eu/esco/skill/14b4a40e-da80-452a-86d6-88a959052219")));
            escoSkills13.add(new EscoSkill(new URL("http://data.europa.eu/esco/skill/2ee57de4-bffd-4194-b009-0060edec77cc")));
        } catch (MalformedURLException e) {
            Log.warn("Wrong ESCO URL", e);
        }
        topic13.setLearningOutcomes(learningOutcomes13);
        topic13.setEscoSkills(escoSkills13);
        topics.add(topic13);

        // 14 Elektrotechnik, ein Überblick
        Topic topic14 = new Topic();
        topic14.setTitle("Elektrotechnik, ein Überblick");
        List<String> learningOutcomes14 = new ArrayList<>();
        List<EscoSkill> escoSkills14 = new ArrayList<>();
        learningOutcomes14.add("Den Einfluss von Strom, Spannung und elektrischem Widerstand umreißen und jeden dieser elektronischen Größen definieren können");
        learningOutcomes14.add("Die Arbeitsweise der wichtigsten Komponenten von Schweißstromquellen umreißen können.");
        learningOutcomes14.add("Die wesentlichen Unterschiede zwischen Gleich- und Wechselstrom beschreiben und Beispiele ihrer individuellen Anwendung bei unterschiedlichen Schweißprozessen nennen können.");
        learningOutcomes14.add("Das Wissen über Elektrizität und Elektronik bezogen auf schweißtechnische Anwendungen nachweisen und anwenden können.");
        try {
            escoSkills14.add(new EscoSkill(new URL("http://data.europa.eu/esco/skill/57869946-cec2-4121-981c-84c9846c58d0")));
            escoSkills14.add(new EscoSkill(new URL("http://data.europa.eu/esco/skill/3e40c7d0-0e36-4b33-bc33-0aa87eda0561")));
        } catch (MalformedURLException e) {
            Log.warn("Wrong ESCO URL", e);
        }
        topic14.setLearningOutcomes(learningOutcomes14);
        topic14.setEscoSkills(escoSkills14);
        topics.add(topic14);

        // 15 Einführung in das Schutzgasschweißen
        Topic topic15 = new Topic();
        topic15.setTitle("Einführung in das Schutzgasschweißen");
        List<String> learningOutcomes15 = new ArrayList<>();
        List<EscoSkill> escoSkills15 = new ArrayList<>();
        learningOutcomes15.add("Einen elektrischen Lichtbogen, seine wesentlichen Bereiche und ihre Bedeutung für das Schweißen und die Lichtbogenstabilität beschreiben können.");
        learningOutcomes15.add("Die Wärmeentwicklung durch einen Lichtbogen umreißen können.");
        learningOutcomes15.add("Beispiele für den Einfluss magnetischer Felder auf den elektrischen Lichtbogen nennen können.");
        learningOutcomes15.add("Geeignete Lösungen umreißen können, wie man Probleme der magnetischen Ablenkung löst.");
        learningOutcomes15.add("Die Lichtbogeneigenschaften bei Gleich- und Wechselstrom beschreiben können.");
        try {
            escoSkills15.add(new EscoSkill(new URL("http://data.europa.eu/esco/skill/27d26fd6-5795-4f0a-884e-112df7c68314")));
        } catch (MalformedURLException e) {
            Log.warn("Wrong ESCO URL", e);
        }
        topic15.setLearningOutcomes(learningOutcomes15);
        topic15.setEscoSkills(escoSkills15);
        topics.add(topic15);

        // 16 WIG-Schweißen
        Topic topic16 = new Topic();
        topic16.setTitle("WIG-Schweißen");
        List<String> learningOutcomes16 = new ArrayList<>();
        List<EscoSkill> escoSkills16 = new ArrayList<>();
        learningOutcomes16.add("Die Arbeitsweise der einzelnen Stromquellen für das Lichtbogenschweißen mit Gleich- und Wechselstrom einschließlich der gebräuchlichsten Zusatzvorrichtungen umreißen können.");
        learningOutcomes16.add("Die statischen und dynamischen Kennlinien, den Arbeitspunkt sowie die Steuerung der Lichtbogenstabilität für alle Schweißstromquellen zum Lichtbogenschweißen beschreiben können.");
        learningOutcomes16.add("Die Bedeutung von Leerlaufspannung, Kurzschlussstrom, Einschaltdauer einer Stromquelle, Spannungsverlusten sowie der Beziehung zwischen Strom und Kabelquerschnitt umreißen können.");
        learningOutcomes16.add("Für jeden Lichtbogenschweißprozess die geeignete Stromquelle ermitteln können.");
        learningOutcomes16.add("Die unterschiedlichen Einstellungen und Schalter der verschiedenen Stromquellen und ihre Wirkungsweise verstehen.");
        try {
            escoSkills16.add(new EscoSkill(new URL("http://data.europa.eu/esco/skill/a51adea8-bf02-488a-a0d8-b0961caca5b0")));
        } catch (MalformedURLException e) {
            Log.warn("Wrong ESCO URL", e);
        }
        topic16.setLearningOutcomes(learningOutcomes16);
        topic16.setEscoSkills(escoSkills16);
        topics.add(topic16);

        // 17 MIG/MAG-Schweißen
        Topic topic17 = new Topic();
        topic17.setTitle("MIG/MAG-Schweißen");
        List<String> learningOutcomes17 = new ArrayList<>();
        List<EscoSkill> escoSkills17 = new ArrayList<>();
        learningOutcomes17.add("Die Grundlagen des MIG/MAG-Schweißens einschließlich des Werkstoffübergangs und ihre Anwendungen beschreiben können.");
        learningOutcomes17.add("Die häufigsten Anwendungen für jede Stromart, Polarität und Elektrode bzw. jeden Schweißzusatz nennen können.");
        learningOutcomes17.add("Den Anwendungsbereich, die geeignete Schweißnahtvorbereitung und die Behebung möglicherweise auftretender Probleme umreißen können.");
        learningOutcomes17.add("Den Einfluss der Schweißparameter auf die Schweißnaht kennen und die geeigneten Schweißparameter für ausgewählte Anwendungen umreißen können.");
        learningOutcomes17.add("Mögliche Gefährdungen und Maßnahmen zum sicheren Umgang und Arbeiten angeben können.");
        learningOutcomes17.add("Die unterschiedlichen Funktionen der wesentlichen Ausrüstungsteile und des Zubehörs umreißen können.");
        learningOutcomes17.add("Die Anwendung der einschlägigen Normen für Verbrauchsmaterial/Schweißzusätze veranschaulichen können.");
        learningOutcomes17.add("Die Auswahl der Verbrauchsmaterialien/Schweißzusätze anhand von Beispielen erläutern können.");
        learningOutcomes17.add("Den Einsatz und die Pflege der Ausrüstung und des Zubehörs vorführen können.");
        try {
            escoSkills17.add(new EscoSkill(new URL("http://data.europa.eu/esco/skill/f015c5d5-6a1c-4f63-9012-a05f4f3410")));
            escoSkills17.add(new EscoSkill(new URL("http://data.europa.eu/esco/skill/2ee57de4-bffd-4194-b009-0060edec77cc")));
        } catch (MalformedURLException e) {
            Log.warn("Wrong ESCO URL", e);
        }
        topic17.setLearningOutcomes(learningOutcomes17);
        topic17.setEscoSkills(escoSkills17);
        topics.add(topic17);

        // 18 Fülldrahtschweißen
        Topic topic18 = new Topic();
        topic18.setTitle("Fülldrahtschweißen");
        List<String> learningOutcomes18 = new ArrayList<>();
        List<EscoSkill> escoSkills18 = new ArrayList<>();
        learningOutcomes18.add("Die Grundlagen des Fülldrahtschweißens einschließlich seiner Anwendungen beschreiben können.");
        learningOutcomes18.add("Die häufigsten Anwendungen für jede Stromart, Polarität und Elektrode bzw. jeden Schweißzusatz nennen können.");
        learningOutcomes18.add("Den Anwendungsbereich, die geeignete Schweißnahtvorbereitung und die Behebung möglicherweise auftretender Probleme erklären können.");
        learningOutcomes18.add("Den Einfluss der Schweißparameter auf die Schweißnaht kennen und die geeigneten Schweißparameter für ausgewählte Anwendungen umreißen können.");
        learningOutcomes18.add("Mögliche Gefährdungen und Maßnahmen zum sicheren Umgang und Arbeiten beschreiben können.");
        learningOutcomes18.add("Die unterschiedlichen Funktionen der wesentlichen Ausrüstungsteile und des Zubehörs umreißen können.");
        learningOutcomes18.add("Die Anwendung der einschlägigen Normen für Verbrauchsmaterialien/Schweißzusätze veranschauli chen können.");
        learningOutcomes18.add("Die Auswahl der Verbrauchsmaterialien/Schweißzusätze anhand von Beispielen erläutern können.");
        learningOutcomes18.add("Über den richtigen Einsatz und die angemessene Pflege der Ausrüstung und des Zubehörs Bescheid wissen.");
        try {
            escoSkills18.add(new EscoSkill(new URL("http://data.europa.eu/esco/skill/27d26fd6-5795-4f0a-884e-112df7c68314")));
            escoSkills18.add(new EscoSkill(new URL("http://data.europa.eu/esco/skill/2ee57de4-bffd-4194-b009-0060edec77cc")));
        } catch (MalformedURLException e) {
            Log.warn("Wrong ESCO URL", e);
        }
        topic18.setLearningOutcomes(learningOutcomes18);
        topic18.setEscoSkills(escoSkills18);
        topics.add(topic18);

        // 19 Lichtbogenhandschweißen
        Topic topic19 = new Topic();
        topic19.setTitle("Lichtbogenhandschweißen");
        List<String> learningOutcomes19 = new ArrayList<>();
        List<EscoSkill> escoSkills19 = new ArrayList<>();
        learningOutcomes19.add("Die Grundlagen des Lichtbogenhandschweißens einschließlich der Sonderverfahren, Lichtbogenzündungstechniken und deren Anwendungen genau erklären können.");
        learningOutcomes19.add("Die Auswahl der für die jeweilige Anwendung geeigneten Stromart, Polarität und Elektrode genau erklären können.");
        learningOutcomes19.add("Den Anwendungsbereich, geeignete Schweißnahtvorbereitungen und möglicherweise zu lösende Probleme genau erklären können.");
        learningOutcomes19.add("Geeignete Schweißparameter für ausgewählte Anwendungen herleiten können.");
        learningOutcomes19.add("Mögliche Gefahren vorhersagen und Maßnahmen zum sicheren Umgang und Arbeiten nennen können.");
        learningOutcomes19.add("Den Zweck und die Funktion jedes Ausrüstungsteils und des Zubehörs kennen.");
        learningOutcomes19.add("Die Handhabungs- und Lagerungsanforderungen hinsichtlich der unterschiedlichen Elektrodentypen genau erklären können.");
        learningOutcomes19.add("Die einschlägigen Normen auslegen können.");
        learningOutcomes19.add("Den Einfluss der Elektrodenumhüllung auf den Tropfenübergang und die Eigenschaften des Schweißguts genau erklären können.");
        learningOutcomes19.add("Die unterschiedlichen Bedienungsfelder und Schalter von verschiedenen Schweißstromquellen zum Lichtbogenhandschweißen sowie ihre Auswirkungen auf den Schweißprozess kennen.");
        try {
            escoSkills19.add(new EscoSkill(new URL("http://data.europa.eu/esco/skill/2ee57de4-bffd-4194-b009-0060edec77cc")));
        } catch (MalformedURLException e) {
            Log.warn("Wrong ESCO URL", e);
        }
        topic19.setLearningOutcomes(learningOutcomes19);
        topic19.setEscoSkills(escoSkills19);
        topics.add(topic19);

        // 20 Gefüge und Eigenschaften von Metallen
        Topic topic20 = new Topic();
        topic20.setTitle("Gefüge und Eigenschaften von Metallen");
        List<String> learningOutcomes20 = new ArrayList<>();
        List<EscoSkill> escoSkills20 = new ArrayList<>();
        learningOutcomes20.add("Das Gefüge/die Strukturen reiner Metalle und Legierungen beschreiben können.");
        learningOutcomes20.add("Die grundlegenden mechanischen Eigenschaften von metallen angeben können.");
        learningOutcomes20.add("Den Einfluss der Auftragungsbedingungen auf die Eigenschaften metallischer Werkstoffe umreißen können.");
        try {
            escoSkills20.add(new EscoSkill(new URL("http://data.europa.eu/esco/skill/5b0a8b4d-7c71-43b1-8e52-4c0cfacd7bbd")));
            escoSkills20.add(new EscoSkill(new URL("http://data.europa.eu/esco/skill/7f107a85-d6ac-410f-a995-d635c5aa418b")));
        } catch (MalformedURLException e) {
            Log.warn("Wrong ESCO URL", e);
        }
        topic20.setLearningOutcomes(learningOutcomes20);
        topic20.setEscoSkills(escoSkills20);
        topics.add(topic20);

        // 21 Zustandsschaubilder und Legierungen
        Topic topic21 = new Topic();
        topic21.setTitle("Zustandsschaubilder und Legierungen");
        List<String> learningOutcomes21 = new ArrayList<>();
        List<EscoSkill> escoSkills21 = new ArrayList<>();
        learningOutcomes21.add("Erstarrungsgefüge und Seigerungen anhand entsprechender Beispiele aufzeigen können.");
        learningOutcomes21.add("Die Grundlagen von Verfestigungsmechanismen anhand entsprechender Beispiele umreißen können.");
        learningOutcomes21.add("Den Zusammenhang zwischen Gefüge/Mikrostruktur und Zustandsschaubildern beschreiben können.");
        learningOutcomes21.add("Legierungen und Zustandsschaubilder beschreiben können.");
        try {
            escoSkills21.add(new EscoSkill(new URL("http://data.europa.eu/esco/skill/d1041406-2c24-4177-9b64-406621a8865e")));
        } catch (MalformedURLException e) {
            Log.warn("Wrong ESCO URL", e);
        }
        topic21.setLearningOutcomes(learningOutcomes21);
        topic21.setEscoSkills(escoSkills21);
        topics.add(topic21);

        // 22 Eisen-Kohlenstoff-Legierungen
        Topic topic22 = new Topic();
        topic22.setTitle("Eisen-Kohlenstoff-Legierungen");
        List<String> learningOutcomes22 = new ArrayList<>();
        List<EscoSkill> escoSkills22 = new ArrayList<>();
        learningOutcomes22.add("Eisen-Kohlenstoff-Legierungen bestimmen können.");
        learningOutcomes22.add("Den Einfluss der Kühlgeschwindigkeit und der Härtefähigkeit beschreiben können.");
        learningOutcomes22.add("Wege zur Vermeidung eines grobkörnigen Gefüges beschreiben können.");
        learningOutcomes22.add("Die Wärmebehandlung klassifizieren können.");
        topic22.setLearningOutcomes(learningOutcomes22);
        topic22.setEscoSkills(escoSkills22);
        topics.add(topic22);

        // 23 Herstellung und Klassifizierung der Stähle
        Topic topic23 = new Topic();
        topic23.setTitle("Herstellung und Klassifizierung der Stähle");
        List<String> learningOutcomes23 = new ArrayList<>();
        List<EscoSkill> escoSkills23 = new ArrayList<>();
        learningOutcomes23.add("Umreißen können, wie Stahl durch Walzen und Gießen verarbeitet wird.");
        learningOutcomes23.add("Die Eigenschaften von Stahl-Halbfabrikaten angeben können.");
        learningOutcomes23.add("Die unterschiedlichen Arten von Stählen, insbesondere Baustählen, unterscheiden können.");
        learningOutcomes23.add("Die Normen zur Bezeichnung von Stählen und Walzprodukten anwenden können.");
        learningOutcomes23.add("Die verschiedenen Arten von Prüfunterlagen unterscheiden können.");
        try {
            escoSkills23.add(new EscoSkill(new URL("http://data.europa.eu/esco/skill/7f107a85-d6ac-410f-a995-d635c5aa418b")));
        } catch (MalformedURLException e) {
            Log.warn("Wrong ESCO URL", e);
        }
        topic23.setLearningOutcomes(learningOutcomes23);
        topic23.setEscoSkills(escoSkills23);
        topics.add(topic23);

        // 24 Verhalten von Baustählen beim Schmelzschweißen
        Topic topic24 = new Topic();
        topic24.setTitle("Verhalten von Baustählen beim Schmelzschweißen");
        List<String> learningOutcomes24 = new ArrayList<>();
        List<EscoSkill> escoSkills24 = new ArrayList<>();
        learningOutcomes24.add("Die Temperaturverteilung in Schweißnähten und das infolge des Einlagen- bzw. Mehrlagenschweißens ausgebildete Gefüge beschreiben können.");
        learningOutcomes24.add("Die Auswirkungen der Wärmezufuhr, Abkühlgeschwindigkeit und Mehrlagenschweißung auf die Erstarrung des Schweißguts und das beim Einlagen- bzw. Mehrlagenschweißen jeweils ausgebildete Gefüge erklären können.");
        learningOutcomes24.add("Die Auswirkungen des Schweißnahtschutzes und der Schweißzusatzart auf das Gefüge des Schweißgutes und seine Eigenschaften beim Einlagen- bzw. Mehrlagenschweißen beschreiben können.");
        learningOutcomes24.add("Die Bereiche der Wärmeeinflusszone und die Gründe für Korngrößen- und Gefügeveränderungen sowie deren Auswirkungen auf die Eigenschaften beim Einlagen- bzw. Mehrlagenschweißen beschreiben können.");
        learningOutcomes24.add("Die verschiedenen Aspekte der Schweißeignung umreißen können.");
        try {
            escoSkills24.add(new EscoSkill(new URL("http://data.europa.eu/esco/skill/5b0a8b4d-7c71-43b1-8e52-4c0cfacd7bbd")));
        } catch (MalformedURLException e) {
            Log.warn("Wrong ESCO URL", e);
        }
        topic24.setLearningOutcomes(learningOutcomes24);
        topic24.setEscoSkills(escoSkills24);
        topics.add(topic24);

        // 25 Brüche und unterschiedliche Arten von Brüchen
        Topic topic25 = new Topic();
        topic25.setTitle("Brüche und unterschiedliche Arten von Brüchen");
        List<String> learningOutcomes25 = new ArrayList<>();
        List<EscoSkill> escoSkills25 = new ArrayList<>();
        learningOutcomes25.add("Die Unterschiede zwischen Rissen und Brüchen erkennen können.");
        learningOutcomes25.add("Die Unterschiede zwischen Spröd- und Dehnungsbrüchen erkennen können.");
        learningOutcomes25.add("Die Unterschiede zwischen den einzelnen Bruchtypen erkennen können.");
        topic25.setLearningOutcomes(learningOutcomes25);
        topic25.setEscoSkills(escoSkills25);
        topics.add(topic25);

        // 26 Wärmebehandlung von Grundwerkstoffen und Schweißverbindungen
        Topic topic26 = new Topic();
        topic26.setTitle("Wärmebehandlung von Grundwerkstoffen und Schweißverbindungen");
        List<String> learningOutcomes26 = new ArrayList<>();
        List<EscoSkill> escoSkills26 = new ArrayList<>();
        learningOutcomes26.add("Die wichtigeren Wärmebehandlungen und ihre Zielstellungen umreißen können.");
        learningOutcomes26.add("Die Anforderungen des Regelwerks an die Wärmebehandlung beschreiben können.");
        learningOutcomes26.add("Die Notwendigkeit einer Wärmebehandlung nach dem Schweißen in Abhängigkeit von der Art und Dicke des Stahls, der Anwendung und des Regelwerks erkennen können.");
        try {
            escoSkills26.add(new EscoSkill(new URL("http://data.europa.eu/esco/skill/51f86650-a633-4d4b-baec-88d16d6f202f")));
        } catch (MalformedURLException e) {
            Log.warn("Wrong ESCO URL", e);
        }
        topic26.setLearningOutcomes(learningOutcomes26);
        topic26.setEscoSkills(escoSkills26);
        topics.add(topic26);

        // 27 Baustähle (unlegierte Stähle)
        Topic topic27 = new Topic();
        topic27.setTitle("Baustähle (unlegierte Stähle)");
        List<String> learningOutcomes27 = new ArrayList<>();
        List<EscoSkill> escoSkills27 = new ArrayList<>();
        learningOutcomes27.add("Die Gefügeeigenschaften unlegierter Stähle umreißen können.");
        learningOutcomes27.add("Die einschlägigen Schweißzusätze beschreiben können.");
        learningOutcomes27.add("Geeignete Schweißprozesse bestimmen können.");
        learningOutcomes27.add("Die richtige Anwendung der Normen für Schweißzusätze vorführen können.");
        try {
            escoSkills27.add(new EscoSkill(new URL("http://data.europa.eu/esco/skill/7f107a85-d6ac-410f-a995-d635c5aa418b")));
            escoSkills27.add(new EscoSkill(new URL("http://data.europa.eu/esco/skill/2ee57de4-bffd-4194-b009-0060edec77cc")));
            escoSkills27.add(new EscoSkill(new URL("http://data.europa.eu/esco/skill/2fcd95a4-1f9a-41cf-95a9-4701bde476b4")));
        } catch (MalformedURLException e) {
            Log.warn("Wrong ESCO URL", e);
        }
        topic27.setLearningOutcomes(learningOutcomes27);
        topic27.setEscoSkills(escoSkills27);
        topics.add(topic27);

        // 28 Wissen über die Kontrolle der verschiedenen Schweißprozesse (Gasschweißen und Brennschneiden, Lichtbogenhandschweißen, Wolfram-Inertgasschweißen (WIG), MIG/MAG + Fülldrahtschweißen, sonstige Schweißprozesse).
        Topic topic28 = new Topic();
        topic28.setTitle("Wissen über die Kontrolle der verschiedenen Schweißprozesse (Gasschweißen und Brennschneiden, Lichtbogenhandschweißen, Wolfram-Inertgasschweißen (WIG), MIG/MAG + Fülldrahtschweißen, sonstige Schweißprozesse).");
        List<String> learningOutcomes28 = new ArrayList<>();
        List<EscoSkill> escoSkills28 = new ArrayList<>();
        learningOutcomes28.add("Erwerb von Wissen über die Kontrolle der verschiedenen Schweißprozesse. Den Teilnehmern/innen soll so " +
                "weit wie möglich vermittelt werden, welche Schwierigkeiten und typischen Fehler auftreten können, wenn die " +
                "unterschiedlichen Schweißprozesse fehlerhaft angewendet werden. Die Teilnehmer/innen werden im Rahmen " +
                "der Übungen von erfahrenem Schweißlehrpersonal angeleitet. Die Vorteile virtueller Schweißausbildungssysteme können genutzt werden.");
        try {
            escoSkills28.add(new EscoSkill(new URL("http://data.europa.eu/esco/skill/2ee57de4-bffd-4194-b009-0060edec77cc")));
            escoSkills28.add(new EscoSkill(new URL("http://data.europa.eu/esco/skill/2fcd95a4-1f9a-41cf-95a9-4701bde476b4")));
            escoSkills28.add(new EscoSkill(new URL("http://data.europa.eu/esco/skill/14b4a40e-da80-452a-86d6-88a959052219")));
            escoSkills28.add(new EscoSkill(new URL("http://data.europa.eu/esco/skill/27d26fd6-5795-4f0a-884e-112df7c68314")));
            escoSkills28.add(new EscoSkill(new URL("http://data.europa.eu/esco/skill/a51adea8-bf02-488a-a0d8-b0961caca5b0")));
            escoSkills28.add(new EscoSkill(new URL("http://data.europa.eu/esco/skill/f015c5d5-6a1c-4f63-9012-a05f4f3410")));
            escoSkills28.add(new EscoSkill(new URL("http://data.europa.eu/esco/skill/43032597-223c-4559-bdf9-abf0a312839a")));
            escoSkills28.add(new EscoSkill(new URL("http://data.europa.eu/esco/skill/38a32307-bb1e-4593-8fa2-e31384417649")));
        } catch (MalformedURLException e) {
            Log.warn("Wrong ESCO URL", e);
        }
        topic28.setLearningOutcomes(learningOutcomes28);
        topic28.setEscoSkills(escoSkills28);
        topics.add(topic28);

        // 29 Der Lichtbogen
        Topic topic29 = new Topic();
        topic29.setTitle("Der Lichtbogen");
        List<String> learningOutcomes29 = new ArrayList<>();
        List<EscoSkill> escoSkills29 = new ArrayList<>();
        learningOutcomes29.add("Einen elektrischen Lichtbogen, seine wesentlichen Bereiche und ihre Bedeutung für das Schweißen und die Lichtbogenstabilität beschreiben können.");
        learningOutcomes29.add("Die Wärmeentwicklung durch einen Lichtbogen umreißen können.");
        learningOutcomes29.add("Beispiele für den Einfluss magnetischer Felder auf den elektrischen Lichtbogen nennen können.");
        learningOutcomes29.add("Geeignete Lösungen umreißen können, wie man Probleme der magnetischen Ablenkung löst.");
        learningOutcomes29.add("Die Lichtbogeneigenschaften bei Gleich- und Wechselstrom beschreiben können.");
        try {
            escoSkills29.add(new EscoSkill(new URL("http://data.europa.eu/esco/skill/57869946-cec2-4121-981c-84c9846c58d0")));
        } catch (MalformedURLException e) {
            Log.warn("Wrong ESCO URL", e);
        }
        topic29.setLearningOutcomes(learningOutcomes29);
        topic29.setEscoSkills(escoSkills29);
        topics.add(topic29);

        // 30 Stromquellen für das Lichtbogenschweißen
        Topic topic30 = new Topic();
        topic30.setTitle("Stromquellen für das Lichtbogenschweißen");
        List<String> learningOutcomes30 = new ArrayList<>();
        List<EscoSkill> escoSkills30 = new ArrayList<>();
        learningOutcomes30.add("Die Arbeitsweise der einzelnen Stromquellen für das Lichtbogenschweißen mit Gleich- und Wechselstrom einschließlich der gebräuchlichsten Zusatzvorrichtungen umreißen können.");
        learningOutcomes30.add("Die statischen und dynamischen Kennlinien, den Arbeitspunkt sowie die Steuerung der Lichtbogenstabilität für alle Schweißstromquellen zum Lichtbogenschweißen beschreiben können.");
        learningOutcomes30.add("Die Bedeutung von Leerlaufspannung, Kurzschlussstrom, Einschaltdauer einer Stromquelle, Spannungsverlusten sowie der Beziehung zwischen Strom und Kabelquerschnitt umreißen können.");
        learningOutcomes30.add("Für jeden Lichtbogenschweißprozess die geeignete Stromquelle ermitteln können.");
        learningOutcomes30.add("Die unterschiedlichen Einstellungen und Schalter der verschiedenen Stromquellen und ihre Wirkungsweise verstehen.");
        try {
            escoSkills30.add(new EscoSkill(new URL("http://data.europa.eu/esco/skill/2ee57de4-bffd-4194-b009-0060edec77cc")));
            escoSkills30.add(new EscoSkill(new URL("http://data.europa.eu/esco/skill/2fcd95a4-1f9a-41cf-95a9-4701bde476b4")));
        } catch (MalformedURLException e) {
            Log.warn("Wrong ESCO URL", e);
        }
        topic30.setLearningOutcomes(learningOutcomes30);
        topic30.setEscoSkills(escoSkills30);
        topics.add(topic30);

        // 31 Unterpulverschweißen
        Topic topic31 = new Topic();
        topic31.setTitle("Unterpulverschweißen");
        List<String> learningOutcomes31 = new ArrayList<>();
        List<EscoSkill> escoSkills31 = new ArrayList<>();
        learningOutcomes31.add("Die Grundlagen des UP-Schweißens einschließlich der Lichtbogenzündungstechniken und deren Anwendungen umreißen können.");
        learningOutcomes31.add("Die Kriterien zur Bestimmung der geeigneten Schweißparameter kennen.");
        learningOutcomes31.add("Den Anwendungsbereich, die Nahtkantenvorbereitung und möglicherweise zu lösende Probleme bezeichnen können.");
        learningOutcomes31.add("Die Verfahren zum Aufbau der Schweißstromquellen vorführen können.");
        learningOutcomes31.add("Die Kriterien zur Auswahl der Pulver-Draht-Kombinationen erklären können.");
        learningOutcomes31.add("Die einschlägigen Normen und Schweißverfahren anwenden können.");
        learningOutcomes31.add("Schweißanweisungen für Schweißer und Bediener nachvollziehen können.");
        learningOutcomes31.add("Mögliche Gefährdungen und Maßnamen zum sicheren Umgang und Arbeiten kennen.");
        try {
            escoSkills31.add(new EscoSkill(new URL("http://data.europa.eu/esco/skill/2fcd95a4-1f9a-41cf-95a9-4701bde476b4")));
        } catch (MalformedURLException e) {
            Log.warn("Wrong ESCO URL", e);
        }
        topic31.setLearningOutcomes(learningOutcomes31);
        topic31.setEscoSkills(escoSkills31);
        topics.add(topic31);

        // 32 Widerstandspunktschweißen
        Topic topic32 = new Topic();
        topic32.setTitle("Widerstandspunktschweißen");
        List<String> learningOutcomes32 = new ArrayList<>();
        List<EscoSkill> escoSkills32 = new ArrayList<>();
        learningOutcomes32.add("Die Grundlagen des Widerstandsschweißens und die Anwendung der verschiedenen Varianten umreißen können.");
        learningOutcomes32.add("Die Auswahl geeigneter Schweißparameter zur Erstellung fehlerfreier Nähte erklären können.");
        learningOutcomes32.add("Die Kriterien zur Auswahl des richtigen Anpressdrucks und der richtigen Stromzyklen nennen können.");
        learningOutcomes32.add("Den Einfluss der Oberflächenbeschaffenheit auf die Endqualität der Schweißverbindung erörtern sowie die Ursachen und Vermeidung häufiger Fehler erklären können.");
        learningOutcomes32.add("Die richtige Anwendung geltender Normen und schweißtechnischer Verfahren vorführen können.");
        learningOutcomes32.add("Schweißanweisungen für Schweißer und Bediener nachvollziehen können.");
        learningOutcomes32.add("Mögliche Gefährdungen und Maßnahmen zum sicheren Umgang und Arbeiten erläutern können.");
        try {
            escoSkills32.add(new EscoSkill(new URL("http://data.europa.eu/esco/skill/2fcd95a4-1f9a-41cf-95a9-4701bde476b4")));
            escoSkills32.add(new EscoSkill(new URL("http://data.europa.eu/esco/skill/38a32307-bb1e-4593-8fa2-e31384417649")));
            escoSkills32.add(new EscoSkill(new URL("http://data.europa.eu/esco/skill/2ee57de4-bffd-4194-b009-0060edec77cc")));
        } catch (MalformedURLException e) {
            Log.warn("Wrong ESCO URL", e);
        }
        topic32.setLearningOutcomes(learningOutcomes32);
        topic32.setEscoSkills(escoSkills32);
        topics.add(topic32);

        // 33 Sonderschweißprozesse-Laserstrahl-,Elektronenstrahl-, Plasmaschweißen
        Topic topic33 = new Topic();
        topic33.setTitle("Sonderschweißprozesse-Laserstrahl-,Elektronenstrahl-, Plasmaschweißen");
        List<String> learningOutcomes33 = new ArrayList<>();
        List<EscoSkill> escoSkills33 = new ArrayList<>();
        learningOutcomes33.add("Die Grundlagen der genannten Prozesse und deren Anwendungen umreißen können.");
        learningOutcomes33.add("Die üblichen prozessspezifischen Anwendungen in den unterschiedlichen Industriebereichen bestimmen können.");
        learningOutcomes33.add("Für jeden Prozess die geeigneten Schweißparameter festlegen und beschreiben können.");
        learningOutcomes33.add("Mögliche Gefährdungen und Maßnahmen zum sicheren Umgang und Arbeiten angeben können.");
        try {
            escoSkills33.add(new EscoSkill(new URL("http://data.europa.eu/esco/skill/5d9d5582-2a36-4887-98fc-cff1ba767983")));
            escoSkills33.add(new EscoSkill(new URL("http://data.europa.eu/esco/skill/8ed0f653-795d-422b-8856-a55ec250bb88")));
            escoSkills33.add(new EscoSkill(new URL("http://data.europa.eu/esco/skill/aa9eb826-390c-4923-b379-ba56444e680e")));
            escoSkills33.add(new EscoSkill(new URL("http://data.europa.eu/esco/skill/fed2fe77-623b-42f7-9d46-a2e5454f069a")));
            escoSkills33.add(new EscoSkill(new URL("http://data.europa.eu/esco/skill/2ee57de4-bffd-4194-b009-0060edec77cc")));
        } catch (MalformedURLException e) {
            Log.warn("Wrong ESCO URL", e);
        }
        topic33.setLearningOutcomes(learningOutcomes33);
        topic33.setEscoSkills(escoSkills33);
        topics.add(topic33);

        // 34 Sonstige Schweißprozesse
        Topic topic34 = new Topic();
        topic34.setTitle("Sonstige Schweißprozesse");
        List<String> learningOutcomes34 = new ArrayList<>();
        List<EscoSkill> escoSkills34 = new ArrayList<>();
        learningOutcomes34.add("Die Grundlagen der genannten Prozesse und deren Anwendungen umreißen können.");
        learningOutcomes34.add("Die üblichen prozessspezifischen Anwendungen in den unterschiedlichen Industriebereichen bestimmen können.");
        learningOutcomes34.add("Mögliche Gefährdungen und Maßnahmen zum sicheren Umgang und Arbeiten beschreiben können.");
        learningOutcomes34.add("Den Einsatz und die Pflege der Ausrüstung und des Zubehörs für die unterschiedlichen Prozesse vorführen können");
        try {
            escoSkills34.add(new EscoSkill(new URL("http://data.europa.eu/esco/skill/c3e54923-0fd7-4024-840f-8add2d85d08c")));
            escoSkills34.add(new EscoSkill(new URL("http://data.europa.eu/esco/skill/8ed0f653-795d-422b-8856-a55ec250bb88")));
            escoSkills34.add(new EscoSkill(new URL("http://data.europa.eu/esco/skill/050dc15e-a41f-4958-bb2c-3fa46c2182be")));
            escoSkills34.add(new EscoSkill(new URL("http://data.europa.eu/esco/skill/2fcd95a4-1f9a-41cf-95a9-4701bde476b4")));
            escoSkills34.add(new EscoSkill(new URL("http://data.europa.eu/esco/skill/2ee57de4-bffd-4194-b009-0060edec77cc")));
        } catch (MalformedURLException e) {
            Log.warn("Wrong ESCO URL", e);
        }
        topic34.setLearningOutcomes(learningOutcomes34);
        topic34.setEscoSkills(escoSkills34);
        topics.add(topic34);

        // 35 Schneiden, Bohren und andere Nahtvorbereitungsverfahren
        Topic topic35 = new Topic();
        topic35.setTitle("Schneiden, Bohren und andere Nahtvorbereitungsverfahren");
        List<String> learningOutcomes35 = new ArrayList<>();
        List<EscoSkill> escoSkills35 = new ArrayList<>();
        learningOutcomes35.add("Die Grundlagen des mechanischen Trennens, autogenen Brennschneidens, Lichtbogen-, Plasma-, Elektronenstrahl- und Wasserstrahlschneidens umreißen können.");
        learningOutcomes35.add("Die kennzeichnenden Parameter für die genannten Prozesse angeben können.");
        learningOutcomes35.add("Die verschiedenen Kantenvorbereitungsverfahren unter Berücksichtigung technischer und wirtschaftlicher Aspekte klassifizieren können.");
        learningOutcomes35.add("Mögliche Risiken und Gefährdungen hinsichtlich der Kantenvorbereitungsverfahren nennen können.");
        try {
            escoSkills35.add(new EscoSkill(new URL("http://data.europa.eu/esco/skill/2973b3bc-5893-4b3d-8953-22ef677a162f")));
            escoSkills35.add(new EscoSkill(new URL("http://data.europa.eu/esco/skill/1aa8b15c-385b-4ee0-963c-b9e57ad6c682")));
            escoSkills35.add(new EscoSkill(new URL("http://data.europa.eu/esco/skill/8b09c290-c941-4119-870f-bdafbd78c669")));
            escoSkills35.add(new EscoSkill(new URL("http://data.europa.eu/esco/skill/d7cab350-7eba-41cf-9c35-827b74541ce8")));
            escoSkills35.add(new EscoSkill(new URL("http://data.europa.eu/esco/skill/f1e122e5-24a8-44c0-bf3f-90044e72370c")));
            escoSkills35.add(new EscoSkill(new URL("http://data.europa.eu/esco/skill/45bef06d-dba9-4192-9805-2f7d27523638")));
        } catch (MalformedURLException e) {
            Log.warn("Wrong ESCO URL", e);
        }
        topic35.setLearningOutcomes(learningOutcomes35);
        topic35.setEscoSkills(escoSkills35);
        topics.add(topic35);

        // 36 Beschichtungsverfahren
        Topic topic36 = new Topic();
        topic36.setTitle("Beschichtungsverfahren");
        List<String> learningOutcomes36 = new ArrayList<>();
        List<EscoSkill> escoSkills36 = new ArrayList<>();
        learningOutcomes36.add("Die Eigenschaften der gängigsten Plattierungsverfahren und thermischen Spritzverfahren umreißen können.");
        learningOutcomes36.add("Den Einfluss der Oberflächenvorbehandlung auf die Spritzverfahren beschreiben können.");
        learningOutcomes36.add("Die gebräuchlichsten thermischen Spritzverfahren und ihre industriellen Anwendungen beschreiben können.");
        learningOutcomes36.add("Mit den genannten Verfahren zusammenhängende Risiken und Gefährdungen kennen.");
        try {
            escoSkills36.add(new EscoSkill(new URL("http://data.europa.eu/esco/skill/ee762b34-5a0e-4dc6-b386-5930bb3e3c25")));
        } catch (MalformedURLException e) {
            Log.warn("Wrong ESCO URL", e);
        }
        topic36.setLearningOutcomes(learningOutcomes36);
        topic36.setEscoSkills(escoSkills36);
        topics.add(topic36);

        // 37 Vollmechanisierte Prozesse und Roboterschweißen
        Topic topic37 = new Topic();
        topic37.setTitle("Vollmechanisierte Prozesse und Roboterschweißen");
        List<String> learningOutcomes37 = new ArrayList<>();
        List<EscoSkill> escoSkills37 = new ArrayList<>();
        learningOutcomes37.add("Lösungen für eine höhere Produktivität beim Schweißen durch Einsatz der Robotertechnik, Automatisierung oder Mechanisierung ermitteln können.");
        learningOutcomes37.add("Die Unterschiede zwischen Offline- und Online-Programmierung erklären können.");
        learningOutcomes37.add("Die Grundlagen und Anwendungen der verschiedenen Arten von Nahtverfolgungssystemen erklären können.");
        learningOutcomes37.add("Die Grundlagen und Anwendungen des Engspalt- und Orbitalschweißens erklären können.");
        learningOutcomes37.add("Die unterschiedlichen prozessspezifischen Anwendungen des Engspalt- und Orbitalschweißens vergleichen können");
        learningOutcomes37.add("Mögliche Risiken, Gefährdungen und Maßnahmen zum sicheren Umgang und Arbeiten angeben können.");
        learningOutcomes37.add("Den geeigneten Robotertyp für den jeweiligen Anwendungsbereich erklären können.");
        try {
            escoSkills37.add(new EscoSkill(new URL("http://data.europa.eu/esco/skill/0f5374e3-0b9b-4b16-af7a-49654ce0bb15")));
            escoSkills37.add(new EscoSkill(new URL("http://data.europa.eu/esco/skill/8006596d-605b-4088-a3f6-8e3aedec21ab")));
        } catch (MalformedURLException e) {
            Log.warn("Wrong ESCO URL", e);
        }
        topic37.setLearningOutcomes(learningOutcomes37);
        topic37.setEscoSkills(escoSkills37);
        topics.add(topic37);

        // 38 Hart- und Weichlöten
        Topic topic38 = new Topic();
        topic38.setTitle("Hart- und Weichlöten");
        List<String> learningOutcomes38 = new ArrayList<>();
        List<EscoSkill> escoSkills38 = new ArrayList<>();
        learningOutcomes38.add("Die verschiedenen Hart- und Weichlötprozesse umreißen können.");
        learningOutcomes38.add("Die verschiedenen Hart- und Weichlötprozesse mit dem Schmelzschweißen vergleichen können.");
        learningOutcomes38.add("Die maßgebenden Arbeitstechniken in Bezug auf das Hart- und Weichlöten darstellen können.");
        learningOutcomes38.add("Die wichtigsten Anwendungen der einzelnen Hart- und Schweißlötprozesse umreißen können.");
        learningOutcomes38.add("Den Einfluss der Oberflächenvorbereitung beim Hart- und Weichlöten beschreiben können.");
        learningOutcomes38.add("Den Einfluss der Oberflächenvorbereitung beim Hart- und Weichlöten beschreiben können.");
        learningOutcomes38.add("Die Arten und Eigenschaften der verwendeten Lote und Flussmittel angeben können.");
        learningOutcomes38.add("Mögliche Risiken, Gefährdungen und Maßnahmen zum Sicheren Umgang und Arbeiten angeben können.");
        try {
            escoSkills38.add(new EscoSkill(new URL("http://data.europa.eu/esco/skill/43032597-223c-4559-bdf9-abf0a312839a")));
            escoSkills38.add(new EscoSkill(new URL("http://data.europa.eu/esco/skill/136596a4-00b5-4ed3-bb6e-717198c1ccbe")));
            escoSkills38.add(new EscoSkill(new URL("http://data.europa.eu/esco/skill/f30a7ea4-77dd-452c-8a2d-b06c12606a7f")));
            escoSkills38.add(new EscoSkill(new URL("http://data.europa.eu/esco/skill/8006596d-605b-4088-a3f6-8e3aedec21ab")));
        } catch (MalformedURLException e) {
            Log.warn("Wrong ESCO URL", e);
        }
        topic38.setLearningOutcomes(learningOutcomes38);
        topic38.setEscoSkills(escoSkills38);
        topics.add(topic38);

        // 39 Kunststofffügen
        Topic topic39 = new Topic();
        topic39.setTitle("Kunststofffügen");
        List<String> learningOutcomes39 = new ArrayList<>();
        List<EscoSkill> escoSkills39 = new ArrayList<>();
        learningOutcomes39.add("Die grundlegenden Eigenschaften und den Anwendungsbereich der einzelnen Fügeprozesse umreißen können.");
        learningOutcomes39.add("Wenn möglich, die in demselben industriellen Anwendungsbereich eingesetzten Prozesse zum Kunststofffügen und Schmelzschweißen metallischer Werkstoffe (z.B. Rohrleitungen aus PE / gewöhnlichem Kohlenstoffstahl) vergleichen können.");
        learningOutcomes39.add("Das Arbeitsprinzip der gebräuchlichsten Fügeprozesse umreißen können.");
        learningOutcomes39.add("Den Stand der Technik in Bezug auf industrielle Anwendungen der einzelnen Fügeprozesse erläutern können.");
        learningOutcomes39.add("Die mit dem Kunststofffügen verbundenen Risiken und Gefährdungen erläutern können.");
        try {
            escoSkills39.add(new EscoSkill(new URL("http://data.europa.eu/esco/skill/4e8ce455-9dbb-4ada-b79b-2f89021f4947")));
        } catch (MalformedURLException e) {
            Log.warn("Wrong ESCO URL", e);
        }
        topic39.setLearningOutcomes(learningOutcomes39);
        topic39.setEscoSkills(escoSkills39);
        topics.add(topic39);

        // 40 Laborübungen
        Topic topic40 = new Topic();
        topic40.setTitle("Laborübungen");
        List<String> learningOutcomes40 = new ArrayList<>();
        List<EscoSkill> escoSkills40 = new ArrayList<>();
        learningOutcomes40.add("Die Form und das Gefüge der Schweißraupe in Abhängigkeit von den verwendeten Schweißparameter umreißen können.");
        learningOutcomes40.add("Den anwendbaren Verfahren entsprechend Bescheid wissen über die jeweils verwendeten  Schweißparameter sowie deren Einstellung, Auswirkung und Überwachung während des Schweißvorgangs.");
        learningOutcomes40.add("Sich wahrscheinlich ergebende Schweißnahtunregelmäßigkeiten und -mängel unter Berücksichtigung des gewählten Schweißprozesses und der verwendeten Schweißparameter beschreiben können.");
        topic40.setLearningOutcomes(learningOutcomes40);
        topic40.setEscoSkills(escoSkills40);
        topics.add(topic40);

        // 41 Rissbildung in Schweißverbindungen
        Topic topic41 = new Topic();
        topic41.setTitle("Rissbildung in Schweißverbindungen");
        List<String> learningOutcomes41 = new ArrayList<>();
        List<EscoSkill> escoSkills41 = new ArrayList<>();
        learningOutcomes41.add("Die metallurgischen Entstehungsmechanismen für alle wesentlichen Rissbildungsarbeiten bestimmen können.");
        learningOutcomes41.add("Die Auswirkung der chemischen und physikalischen Einflussfaktoren auf alle wesentlichen Rissbil dungsarten beschreiben können.");
        learningOutcomes41.add("Die Rissanfälligkeit einstufen und geeignete Vorbeugungsmaßnahmen zur Rissvermeidung vorschlagen können.");
        learningOutcomes41.add("Die Art der Rissbildung und ihre Ursache nach Untersuchung des gerissenen Werkstoffs und seiner Vorgeschichte bestimmen können.");
        learningOutcomes41.add("Alternativen zur Reduzierung oder Vermeidung von Terrassenbruchbildung in Schweißkonstruktionen angeben können.");
        try {
            escoSkills41.add(new EscoSkill(new URL("http://data.europa.eu/esco/skill/6669458f-8692-4c7c-a1f0-bc9cc5310faa")));
        } catch (MalformedURLException e) {
            Log.warn("Wrong ESCO URL", e);
        }
        topic41.setLearningOutcomes(learningOutcomes41);
        topic41.setEscoSkills(escoSkills41);
        topics.add(topic41);

        // 42 Hochfeste Stähle
        Topic topic42 = new Topic();
        topic42.setTitle("Hochfeste Stähle");
        List<String> learningOutcomes42 = new ArrayList<>();
        List<EscoSkill> escoSkills42 = new ArrayList<>();
        learningOutcomes42.add("Die verschiedenen Verfahren zur Herstellung von Feinkornbaustählen umreißen können.");
        learningOutcomes42.add("Den Zusammenhang zwischen Kornfeinung und mechanischen Eigenschaften beschreiben können.");
        learningOutcomes42.add("Geeignete Anwendungen umreißen können.");
        learningOutcomes42.add("Den Zusammenhang zwischen Güteklasse und Schweißeignung beschreiben können.");
        learningOutcomes42.add("Anwendbare Schweißprozesse und mögliche Probleme benennen können.");
        learningOutcomes42.add("Den Einfluss der Wärmenachbehandlung erklären und die Bedingungen (insbesondere die Temperatur) einer solchen Behandlung herleiten können.");
        learningOutcomes42.add("Den geeigneten Schweißzusatz bestimmen können.");
        try {
            escoSkills42.add(new EscoSkill(new URL("http://data.europa.eu/esco/skill/7f107a85-d6ac-410f-a995-d635c5aa418b")));
        } catch (MalformedURLException e) {
            Log.warn("Wrong ESCO URL", e);
        }
        topic42.setLearningOutcomes(learningOutcomes42);
        topic42.setEscoSkills(escoSkills42);
        topics.add(topic42);

        // 43 Anwendung von Baustählen und hochfesten Stählen
        Topic topic43 = new Topic();
        topic43.setTitle("Anwendung von Baustählen und hochfesten Stählen");
        List<String> learningOutcomes43 = new ArrayList<>();
        List<EscoSkill> escoSkills43 = new ArrayList<>();
        learningOutcomes43.add("Die Bedeutung der anwendungsbezogenen Werkstoffauswahl beschreiben können.");
        learningOutcomes43.add("Die Verwendung von Baustählen und hochfesten Stählen sowie deren Einsatzgebiete umreißen können.");
        learningOutcomes43.add("Beispiele für die praktische Anwendung und Konstruktion von Brücken, Kränen, Druckbehältern und Automobilzubehör, Gebäuden (Architektur), Schiffen und Rohrleitungen usw. nennen können.");
        try {
            escoSkills43.add(new EscoSkill(new URL("http://data.europa.eu/esco/skill/7f107a85-d6ac-410f-a995-d635c5aa418b")));
        } catch (MalformedURLException e) {
            Log.warn("Wrong ESCO URL", e);
        }
        topic43.setLearningOutcomes(learningOutcomes43);
        topic43.setEscoSkills(escoSkills43);
        topics.add(topic43);

        // 44 Kriech- und warmfeste Stähle
        Topic topic44 = new Topic();
        topic44.setTitle("Kriech- und warmfeste Stähle");
        List<String> learningOutcomes44 = new ArrayList<>();
        List<EscoSkill> escoSkills44 = new ArrayList<>();
        learningOutcomes44.add("Die grundlegenden Phänomene und Stadien des Kriechens beschreiben können.");
        learningOutcomes44.add("Den Einfluss von Legierungselementen und Stahlgefügen auf die Warmfestigkeit umreißen können.");
        learningOutcomes44.add("Die Schweißeignung von Cr-Mo-Stählen unter Berücksichtigung geeigneter Schweißprozesse sowie Schweißzusatz- und -hilfsstoffe umreißen können.");
        try {
            escoSkills44.add(new EscoSkill(new URL("http://data.europa.eu/esco/skill/7f107a85-d6ac-410f-a995-d635c5aa418b")));
        } catch (MalformedURLException e) {
            Log.warn("Wrong ESCO URL", e);
        }
        topic44.setLearningOutcomes(learningOutcomes44);
        topic44.setEscoSkills(escoSkills44);
        topics.add(topic44);

        // 45 Stähle für die Tieftemperaturanwendung
        Topic topic45 = new Topic();
        topic45.setTitle("Stähle für die Tieftemperaturanwendung");
        List<String> learningOutcomes45 = new ArrayList<>();
        List<EscoSkill> escoSkills45 = new ArrayList<>();
        learningOutcomes45.add("Den Einfluss von Nickel auf das Tieftemperaturgefüge verstehen.");
        learningOutcomes45.add("Den Einfluss des Nickelgehalts auf die Schweißeignung beschreiben können.");
        learningOutcomes45.add("Den Anwendungsbereich für die unterschiedlichen Tieftemperaturstahlsorten darlegen können.");
        learningOutcomes45.add("Die Schweißeignung unter Berücksichtigung des geeigneten Schweißprozesses und der Arten von Schweißzusätzen umreißen können.");
        try {
            escoSkills45.add(new EscoSkill(new URL("http://data.europa.eu/esco/skill/7f107a85-d6ac-410f-a995-d635c5aa418b")));
        } catch (MalformedURLException e) {
            Log.warn("Wrong ESCO URL", e);
        }
        topic45.setLearningOutcomes(learningOutcomes45);
        topic45.setEscoSkills(escoSkills45);
        topics.add(topic45);

        // 46 Korrosion
        Topic topic46 = new Topic();
        topic46.setTitle("Korrosion");
        List<String> learningOutcomes46 = new ArrayList<>();
        List<EscoSkill> escoSkills46 = new ArrayList<>();
        learningOutcomes46.add("Die bei der Korrosion auftretenden chemischen und elektrochemischen Phänomene umreißen können.");
        learningOutcomes46.add("Die am häufigsten vorkommenden Korrosionsarten erkennen und beschreiben können.");
        learningOutcomes46.add("Beispiele für gebräuchliche Schutzmaßnahmen nennen können.");
        try {
            escoSkills46.add(new EscoSkill(new URL("http://data.europa.eu/esco/skill/b0279338-ddfa-473d-b961-fa51b1185612")));
            escoSkills46.add(new EscoSkill(new URL("http://data.europa.eu/esco/skill/1c5f1010-17aa-4a17-acd3-3bbef4fcd2b4")));
        } catch (MalformedURLException e) {
            Log.warn("Wrong ESCO URL", e);
        }
        topic46.setLearningOutcomes(learningOutcomes46);
        topic46.setEscoSkills(escoSkills46);
        topics.add(topic46);

        // 47 Korrosions- und Hitzebeständige Stähle
        Topic topic47 = new Topic();
        topic47.setTitle("Korrosions- und Hitzebeständige Stähle");
        List<String> learningOutcomes47 = new ArrayList<>();
        List<EscoSkill> escoSkills47 = new ArrayList<>();
        learningOutcomes47.add("Die Gefüge der verschiedenen korrosionsbeständigen Stähle umreißen und ihr Verhalten im Schweißprozess erläutern können.");
        learningOutcomes47.add("Die Regeln und Gesetzmäßigkeiten kennen, welche die Versprödungsphänomene bestimmen.");
        learningOutcomes47.add("Die Auswahl des richtigen Schweißprozesses sowie der richtigen Zusatz- und Hilfsstoffe für alle korrosionsbeständigen Stahlsorten mit Hilfe der verschiedenen Diagramme darlegen können.");
        learningOutcomes47.add("Die unterschiedlichen Schweißnahtnachbehandlungsformen beschreiben können.");
        learningOutcomes47.add("Die Notwendigkeit einer Schweißnahtnachbehandlung erkennen können.");
        try {
            escoSkills47.add(new EscoSkill(new URL("http://data.europa.eu/esco/skill/befd299e-63f3-44a7-8ea4-fc2c6df146d3")));
        } catch (MalformedURLException e) {
            Log.warn("Wrong ESCO URL", e);
        }
        topic47.setLearningOutcomes(learningOutcomes47);
        topic47.setEscoSkills(escoSkills47);
        topics.add(topic47);

        // 48 Verschleiß- und Schutzschichten
        Topic topic48 = new Topic();
        topic48.setTitle("Verschleiß- und Schutzschichten");
        List<String> learningOutcomes48 = new ArrayList<>();
        List<EscoSkill> escoSkills48 = new ArrayList<>();
        learningOutcomes48.add("Verschleißsituationen, welche die Mechanismen der unterschiedlichen Verschleißarten miteinschließen umreißen können.");
        learningOutcomes48.add("Die Grundlagen und Ergebnisse von Prüfungen zur Bestimmung der Verschleißfestigkeit beschreiben können.");
        learningOutcomes48.add("Vorbeugungsmaßnahmen und Verfahren zur Vermeidung übermäßigen Verschleißes umreißen können.");
        learningOutcomes48.add("Die unterschiedlichen Techniken zur Aufbringung von Schutzschichten und die Gründe ihrer Auswahl im Allgemeinen darlegen können.");
        learningOutcomes48.add("Die mit den jeweiligen Methoden verbundenen Probleme und Methoden zu ihrer Lösung umreißen können.");
        try {
            escoSkills48.add(new EscoSkill(new URL("http://data.europa.eu/esco/skill/34250f4b-b39e-42bd-a2d7-783e90051b08")));
            escoSkills48.add(new EscoSkill(new URL("http://data.europa.eu/esco/skill/ee762b34-5a0e-4dc6-b386-5930bb3e3c25")));
        } catch (MalformedURLException e) {
            Log.warn("Wrong ESCO URL", e);
        }
        topic48.setLearningOutcomes(learningOutcomes48);
        topic48.setEscoSkills(escoSkills48);
        topics.add(topic48);

        // 49 Gusseisen und Stahlguss
        Topic topic49 = new Topic();
        topic49.setTitle("Gusseisen und Stahlguss");
        List<String> learningOutcomes49 = new ArrayList<>();
        List<EscoSkill> escoSkills49 = new ArrayList<>();
        learningOutcomes49.add("Die verschiedenen Sorten von Gusseisen und Stahlguss unterscheiden können.");
        learningOutcomes49.add("Probleme im Hinblick auf die Schweißeignung und geeignete Schweißprozesse sowie Arten von Zusatz- werkstoffen für das Schweißen von Gusseisen beschreiben können.");
        try {
            escoSkills49.add(new EscoSkill(new URL("http://data.europa.eu/esco/skill/6669458f-8692-4c7c-a1f0-bc9cc5310faa")));
        } catch (MalformedURLException e) {
            Log.warn("Wrong ESCO URL", e);
        }
        topic49.setLearningOutcomes(learningOutcomes49);
        topic49.setEscoSkills(escoSkills49);
        topics.add(topic49);

        // 50 Kupfer und Kupferlegierungen
        Topic topic50 = new Topic();
        topic50.setTitle("Kupfer und Kupferlegierungen");
        List<String> learningOutcomes50 = new ArrayList<>();
        List<EscoSkill> escoSkills50 = new ArrayList<>();
        learningOutcomes50.add("Die Schweißeignung von Kupfer und Kupferlegierungen erklären können.");
        learningOutcomes50.add("Geeignete Schweißprozesse und Arten von Schweißzusätzen für Kupfer und Kupferlegierungen genau beschreiben können.");
        learningOutcomes50.add("Beispiele für Anwendungen von Kupfer und Kupferlegierungen nennen können.");
        try {
            escoSkills50.add(new EscoSkill(new URL("http://data.europa.eu/esco/skill/7f107a85-d6ac-410f-a995-d635c5aa418b")));
            escoSkills50.add(new EscoSkill(new URL("http://data.europa.eu/esco/skill/0b472dad-1a3f-456e-99ba-de5ffe8665c2")));
            escoSkills50.add(new EscoSkill(new URL("http://data.europa.eu/esco/skill/d1041406-2c24-4177-9b64-406621a8865e")));
        } catch (MalformedURLException e) {
            Log.warn("Wrong ESCO URL", e);
        }
        topic50.setLearningOutcomes(learningOutcomes50);
        topic50.setEscoSkills(escoSkills50);
        topics.add(topic50);

        // 51 Nickel und Nickellegierungen
        Topic topic51 = new Topic();
        topic51.setTitle("Nickel und Nickellegierungen");
        List<String> learningOutcomes51 = new ArrayList<>();
        List<EscoSkill> escoSkills51 = new ArrayList<>();
        learningOutcomes51.add("Die Schweißeignung von Nickel und Nickellegierungen umreißen können.");
        learningOutcomes51.add("Geeignete Schweißprozesse und Arten von Schweißzusätzen für Nickel und Nickellegierungen genau bestimmen können.");
        learningOutcomes51.add("Beispiele für die Anwendung von Nickel und Nickellegierungen nennen können.");
        try {
            escoSkills51.add(new EscoSkill(new URL("http://data.europa.eu/esco/skill/7f107a85-d6ac-410f-a995-d635c5aa418b")));
            escoSkills51.add(new EscoSkill(new URL("http://data.europa.eu/esco/skill/0b472dad-1a3f-456e-99ba-de5ffe8665c2")));
            escoSkills51.add(new EscoSkill(new URL("http://data.europa.eu/esco/skill/d1041406-2c24-4177-9b64-406621a8865e")));
        } catch (MalformedURLException e) {
            Log.warn("Wrong ESCO URL", e);
        }
        topic51.setLearningOutcomes(learningOutcomes51);
        topic51.setEscoSkills(escoSkills51);
        topics.add(topic51);

        // 52 Aluminium und Aluminiumlegierungen Grundlagen
        Topic topic52 = new Topic();
        topic52.setTitle("Aluminium und Aluminiumlegierungen Grundlagen");
        List<String> learningOutcomes52 = new ArrayList<>();
        List<EscoSkill> escoSkills52 = new ArrayList<>();
        learningOutcomes52.add("Die Schweißeignung von Aluminium und Aluminiumlegierungen umreißen können.");
        learningOutcomes52.add("Geeignete Schweißprozesse und Arten von Schweißzusätzen für Aluminium und Aluminiumlegierungen bestimmen können.");
        learningOutcomes52.add("Beispiele für die Anwendung von Aluminium und Aluminiumlegierungen nennen können.");
        try {
            escoSkills52.add(new EscoSkill(new URL("http://data.europa.eu/esco/skill/7f107a85-d6ac-410f-a995-d635c5aa418b")));
            escoSkills52.add(new EscoSkill(new URL("http://data.europa.eu/esco/skill/0b472dad-1a3f-456e-99ba-de5ffe8665c2")));
            escoSkills52.add(new EscoSkill(new URL("http://data.europa.eu/esco/skill/d1041406-2c24-4177-9b64-406621a8865e")));
            escoSkills52.add(new EscoSkill(new URL("http://data.europa.eu/esco/skill/b413a662-2af0-41c6-8dce-5f83bffc82ea")));
        } catch (MalformedURLException e) {
            Log.warn("Wrong ESCO URL", e);
        }
        topic52.setLearningOutcomes(learningOutcomes52);
        topic52.setEscoSkills(escoSkills52);
        topics.add(topic52);

        // 53 Titan
        Topic topic53 = new Topic();
        topic53.setTitle("Titan");
        List<String> learningOutcomes53 = new ArrayList<>();
        List<EscoSkill> escoSkills53 = new ArrayList<>();
        learningOutcomes53.add("Die Schweißmetallurgie der genannten Metalle umreißen können.");
        learningOutcomes53.add("Die Schweißeignung der genannten Metalle kennen.");
        learningOutcomes53.add("Geeignete Schweißprozesse und einige typische Anwendungen bestimmen können.");
        try {
            escoSkills53.add(new EscoSkill(new URL("http://data.europa.eu/esco/skill/7f107a85-d6ac-410f-a995-d635c5aa418b")));
            escoSkills53.add(new EscoSkill(new URL("http://data.europa.eu/esco/skill/0b472dad-1a3f-456e-99ba-de5ffe8665c2")));
            escoSkills53.add(new EscoSkill(new URL("http://data.europa.eu/esco/skill/d1041406-2c24-4177-9b64-406621a8865e")));
        } catch (MalformedURLException e) {
            Log.warn("Wrong ESCO URL", e);
        }
        topic53.setLearningOutcomes(learningOutcomes53);
        topic53.setEscoSkills(escoSkills53);
        topics.add(topic53);

        // 54 Fügen unterschiedlicher Werkstoffe
        Topic topic54 = new Topic();
        topic54.setTitle("Fügen unterschiedlicher Werkstoffe");
        List<String> learningOutcomes54 = new ArrayList<>();
        List<EscoSkill> escoSkills54 = new ArrayList<>();
        learningOutcomes54.add("Die mit dem Fügen unterschiedlicher Werkstoffe verbundenen metallurgischen Probleme und Probleme der Schweißarbeit umreißen können.");
        learningOutcomes54.add("Die korrekte Anwendung des Schaeffler-/De Long-Diagramm/WRC-Diagramm zur Auswahl der geeigneten Schweißzusätze vorführen können.");
        learningOutcomes54.add("Die schweißtechnischen Methoden im Rahmen typischer Anwendungen auflisten können, welche die metallurgischen Probleme verringern.");
        try {
            escoSkills54.add(new EscoSkill(new URL("http://data.europa.eu/esco/skill/7f107a85-d6ac-410f-a995-d635c5aa418b")));
            escoSkills54.add(new EscoSkill(new URL("http://data.europa.eu/esco/skill/d1041406-2c24-4177-9b64-406621a8865e")));
        } catch (MalformedURLException e) {
            Log.warn("Wrong ESCO URL", e);
        }
        topic54.setLearningOutcomes(learningOutcomes54);
        topic54.setEscoSkills(escoSkills54);
        topics.add(topic54);

        // 55 Zerstörende Prüfung von Werkstoffen und Schweißverbindungen
        Topic topic55 = new Topic();
        topic55.setTitle("Zerstörende Prüfung von Werkstoffen und Schweißverbindungen");
        List<String> learningOutcomes55 = new ArrayList<>();
        List<EscoSkill> escoSkills55 = new ArrayList<>();
        learningOutcomes55.add("Die Ziele zerstörenden Prüfens und einschränkende Faktoren bezüglich der gewonnenen Daten benennen können.");
        learningOutcomes55.add("Alle wesentlichen Prücfverfahren und entsprechenden Messparameter beschreiben können.");
        learningOutcomes55.add("Aussagen darübermachen können, wann und warum ein bestimmtes Prüfverfahren angebracht ist.");
        learningOutcomes55.add("In der Lage sein, Prüfungen nach einem vorgegebenen Plan durchzuführen.");
        try {
            escoSkills55.add(new EscoSkill(new URL("http://data.europa.eu/esco/skill/75697ed4-c1a3-47dc-93e3-bb50f21aa2f6")));
            escoSkills55.add(new EscoSkill(new URL("http://data.europa.eu/esco/skill/8d6a9e62-1e89-49df-8147-680081ce5315")));
            escoSkills55.add(new EscoSkill(new URL("http://data.europa.eu/esco/skill/d48b3cfe-80b6-406f-b582-d959b60417f8")));
        } catch (MalformedURLException e) {
            Log.warn("Wrong ESCO URL", e);
        }
        topic55.setLearningOutcomes(learningOutcomes55);
        topic55.setEscoSkills(escoSkills55);
        topics.add(topic55);

        // 56 Grundlagen der Statik
        Topic topic56 = new Topic();
        topic56.setTitle("Grundlagen der Statik");
        List<String> learningOutcomes56 = new ArrayList<>();
        List<EscoSkill> escoSkills56 = new ArrayList<>();
        learningOutcomes56.add("Das Zusammensetzen von Kräften umreißen können.");
        learningOutcomes56.add("Das Zerlegen von Kräften umreißen können.");
        learningOutcomes56.add("Die Gleichgewichtsbedingungen bestimmen können.");
        learningOutcomes56.add("Das Gleichgewicht von Tragwerken beschreiben können.");
        learningOutcomes56.add("Auflager, Einspannungen und grundlegende Verbindungsarten erkennen können.");
        try {
            escoSkills56.add(new EscoSkill(new URL("http://data.europa.eu/esco/skill/efa141df-f382-418f-9121-bd88fd735669")));
        } catch (MalformedURLException e) {
            Log.warn("Wrong ESCO URL", e);
        }
        topic56.setLearningOutcomes(learningOutcomes56);
        topic56.setEscoSkills(escoSkills56);
        topics.add(topic56);

        // 57 Grundlagen der Festigkeitslehre
        Topic topic57 = new Topic();
        topic57.setTitle("Grundlagen der Festigkeitslehre");
        List<String> learningOutcomes57 = new ArrayList<>();
        List<EscoSkill> escoSkills57 = new ArrayList<>();
        learningOutcomes57.add("Die unterschiedlichen Spannungsarten (Normalspannung, Schubspannung) beschreiben können.");
        learningOutcomes57.add("Die unterschiedlichen Verformungsarten (Dehnung, Gleitung usw.) umreißen können.");
        learningOutcomes57.add("Den Zusammenhang zwischen Spannung und Verformung umreißen können.");
        learningOutcomes57.add("Spannungen aufgrund innerer Kräfte und Momente erklären können.");
        try {
            escoSkills57.add(new EscoSkill(new URL("http://data.europa.eu/esco/skill/82b6b024-6369-4d84-ba68-c665b9df53f8")));
        } catch (MalformedURLException e) {
            Log.warn("Wrong ESCO URL", e);
        }
        topic57.setLearningOutcomes(learningOutcomes57);
        topic57.setEscoSkills(escoSkills57);
        topics.add(topic57);

        // 58 Gestaltung von Schweiß- und Lötverbindungen
        Topic topic58 = new Topic();
        topic58.setTitle("Gestaltung von Schweiß- und Lötverbindungen");
        List<String> learningOutcomes58 = new ArrayList<>();
        List<EscoSkill> escoSkills58 = new ArrayList<>();
        learningOutcomes58.add("Die unterschiedlichen Arten von Schweißverbindungen bestimmen können.");
        learningOutcomes58.add("Die korrekte Anwendung von ISO 9692 gemäß den jeweiligen Bedingungen darlegen können.");
        learningOutcomes58.add("Gängige Schweißsymbole erkennen und anwenden können.");
        learningOutcomes58.add("Die symbolische Darstellung von Schweißnähten sowie Hart- und Weichlötverbindungen auf Zeichnungen nachvollziehen können");
        try {
            escoSkills58.add(new EscoSkill(new URL("http://data.europa.eu/esco/skill/29d762da-1711-4a3c-b424-f0a404f4bf2d")));
            escoSkills58.add(new EscoSkill(new URL("http://data.europa.eu/esco/skill/5133ed7b-0785-4c28-825d-1dcf518c4b5b")));
        } catch (MalformedURLException e) {
            Log.warn("Wrong ESCO URL", e);
        }
        topic58.setLearningOutcomes(learningOutcomes58);
        topic58.setEscoSkills(escoSkills58);
        topics.add(topic58);

        // 59 Grundlagen der schweißtechnischen Gestaltung
        Topic topic59 = new Topic();
        topic59.setTitle("Grundlagen der schweißtechnischen Gestaltung");
        List<String> learningOutcomes59 = new ArrayList<>();
        List<EscoSkill> escoSkills59 = new ArrayList<>();
        learningOutcomes59.add("Die unterschiedlichen Arten von Spannungen in Schweißnähten (Nennspannung, Strukturspannung, Kerbspannung) genau erklären können.");
        learningOutcomes59.add("Einfache Schweißverbindungen beschreiben können (innere Kräfte).");
        learningOutcomes59.add("Schweißnahtquerschnitte beschreiben können.");
        try {
            escoSkills59.add(new EscoSkill(new URL("http://data.europa.eu/esco/skill/2964011e-f828-4c6c-b98b-51e984d66365")));
        } catch (MalformedURLException e) {
            Log.warn("Wrong ESCO URL", e);
        }
        topic59.setLearningOutcomes(learningOutcomes59);
        topic59.setEscoSkills(escoSkills59);
        topics.add(topic59);

        // 60 Verhalten von Schweißkonstruktionen unter verschiedenen Beanspruchungsarten
        Topic topic60 = new Topic();
        topic60.setTitle("Verhalten von Schweißkonstruktionen unter verschiedenen Beanspruchungsarten");
        List<String> learningOutcomes60 = new ArrayList<>();
        List<EscoSkill> escoSkills60 = new ArrayList<>();
        learningOutcomes60.add("Die bauteilspezifischen Anforderungen entsprechend den unterschiedlichen Beanspruchungsarten und Temperaturen umreißen können.");
        learningOutcomes60.add("Werkstoffgruppen, die den Anforderungen an Festigkeit/Temperatur entsprechen, allgemein bestimmen können.");
        learningOutcomes60.add("Die verschiedenen Brucharten bestimmen können.");
        try {
            escoSkills60.add(new EscoSkill(new URL("http://data.europa.eu/esco/skill/2964011e-f828-4c6c-b98b-51e984d66365")));
        } catch (MalformedURLException e) {
            Log.warn("Wrong ESCO URL", e);
        }
        topic60.setLearningOutcomes(learningOutcomes60);
        topic60.setEscoSkills(escoSkills60);
        topics.add(topic60);

        // 61 Gestaltung von Schweißkonstruktionen für vorwiegend ruhende Beanspruchung
        Topic topic61 = new Topic();
        topic61.setTitle("Gestaltung von Schweißkonstruktionen für vorwiegend ruhende Beanspruchung");
        List<String> learningOutcomes61 = new ArrayList<>();
        List<EscoSkill> escoSkills61 = new ArrayList<>();
        learningOutcomes61.add("Die unterschiedlichen Anschlüsse bestimmen können.");
        learningOutcomes61.add("Die Bedeutung unterschiedlicher Schweißnahtgeometrien beschreiben können.");
        learningOutcomes61.add("Spannungen in Bauteilen bestimmen können.");
        learningOutcomes61.add("Die Vor- und Nachteile unterschiedlicher Schweißnahtarten beschreiben können.");
        try {
            escoSkills61.add(new EscoSkill(new URL("http://data.europa.eu/esco/skill/2964011e-f828-4c6c-b98b-51e984d66365")));
        } catch (MalformedURLException e) {
            Log.warn("Wrong ESCO URL", e);
        }
        topic61.setLearningOutcomes(learningOutcomes61);
        topic61.setEscoSkills(escoSkills61);
        topics.add(topic61);

        // 62 Verhalten von Schweißkonstruktionen unter zyklischer Beanspruchung
        Topic topic62 = new Topic();
        topic62.setTitle("Verhalten von Schweißkonstruktionen unter zyklischer Beanspruchung");
        List<String> learningOutcomes62 = new ArrayList<>();
        List<EscoSkill> escoSkills62 = new ArrayList<>();
        learningOutcomes62.add("Ein Wöhler-Schaubild anwenden können.");
        learningOutcomes62.add("Den Einfluss von Kerben und Schweißnahtunregelmäßigkeiten beschreiben können.");
        learningOutcomes62.add("Mögliche Änderungen an den Schweißnähten zur Verbesserung von deren Beanspruchbarkeit beschreiben können.");
        try {
            escoSkills62.add(new EscoSkill(new URL("http://data.europa.eu/esco/skill/2964011e-f828-4c6c-b98b-51e984d66365")));
            escoSkills62.add(new EscoSkill(new URL("http://data.europa.eu/esco/skill/632843be-2872-47b4-a92e-352629f9bd88")));
        } catch (MalformedURLException e) {
            Log.warn("Wrong ESCO URL", e);
        }
        topic62.setLearningOutcomes(learningOutcomes62);
        topic62.setEscoSkills(escoSkills62);
        topics.add(topic62);

        // 63 Gestaltung zyklisch beanspruchter Schweißkonstruktionen
        Topic topic63 = new Topic();
        topic63.setTitle("Gestaltung zyklisch beanspruchter Schweißkonstruktionen");
        List<String> learningOutcomes63 = new ArrayList<>();
        List<EscoSkill> escoSkills63 = new ArrayList<>();
        learningOutcomes63.add("Die Grundsätze für die Gestaltung von Schweißkonstruktionen kennen.");
        learningOutcomes63.add("Die Gestaltung von Schweißverbindungen entsprechend den Gegebenheiten beschreiben können.");
        learningOutcomes63.add("Den Einfluss von Kerbwirkungen auf die Klassifizierung von Schweißverbindungen nachvollziehen können.");
        try {
            escoSkills63.add(new EscoSkill(new URL("http://data.europa.eu/esco/skill/2964011e-f828-4c6c-b98b-51e984d66365")));
            escoSkills63.add(new EscoSkill(new URL("http://data.europa.eu/esco/skill/632843be-2872-47b4-a92e-352629f9bd88")));
        } catch (MalformedURLException e) {
            Log.warn("Wrong ESCO URL", e);
        }
        topic63.setLearningOutcomes(learningOutcomes63);
        topic63.setEscoSkills(escoSkills63);
        topics.add(topic63);

        // 64 Gestaltung geschweißter Druckgeräte
        Topic topic64 = new Topic();
        topic64.setTitle("Gestaltung geschweißter Druckgeräte");
        List<String> learningOutcomes64 = new ArrayList<>();
        List<EscoSkill> escoSkills64 = new ArrayList<>();
        learningOutcomes64.add("Die Vorteile der verschiedenen Schweißnahtdetails umreißen können.");
        learningOutcomes64.add("Die Gestaltung bestimmter konstruktiver Schweißnahtdetails darstellen können.");
        try {
            escoSkills64.add(new EscoSkill(new URL("http://data.europa.eu/esco/skill/2964011e-f828-4c6c-b98b-51e984d66365")));
            escoSkills64.add(new EscoSkill(new URL("http://data.europa.eu/esco/skill/632843be-2872-47b4-a92e-352629f9bd88")));
        } catch (MalformedURLException e) {
            Log.warn("Wrong ESCO URL", e);
        }
        topic64.setLearningOutcomes(learningOutcomes64);
        topic64.setEscoSkills(escoSkills64);
        topics.add(topic64);


        // 65 Gestaltung geschweißter Aluminiumkonstruktionen
        Topic topic65 = new Topic();
        topic65.setTitle("Gestaltung geschweißter Aluminiumkonstruktionen");
        List<String> learningOutcomes65 = new ArrayList<>();
        List<EscoSkill> escoSkills65 = new ArrayList<>();
        learningOutcomes65.add("Typische Verbindungsformen und Nahtvorbereitungen für Aluminium nennen können.");
        learningOutcomes65.add("Die bei Aluminium häufigen Fehler und Möglichkeiten zu ihrer Vermeidung kennen.");
        learningOutcomes65.add("Typische Anwendungen und die Vorteile im Vergleich mit Stahlkonstruktionen benennen können.");
        try {
            escoSkills65.add(new EscoSkill(new URL("http://data.europa.eu/esco/skill/2964011e-f828-4c6c-b98b-51e984d66365")));
            escoSkills65.add(new EscoSkill(new URL("http://data.europa.eu/esco/skill/632843be-2872-47b4-a92e-352629f9bd88")));
        } catch (MalformedURLException e) {
            Log.warn("Wrong ESCO URL", e);
        }
        topic65.setLearningOutcomes(learningOutcomes65);
        topic65.setEscoSkills(escoSkills65);
        topics.add(topic65);

        // 66 Einführung in die Qualitätssicherung geschweißter Konstruktionen
        Topic topic66 = new Topic();
        topic66.setTitle("Einführung in die Qualitätssicherung geschweißter Konstruktionen");
        List<String> learningOutcomes66 = new ArrayList<>();
        List<EscoSkill> escoSkills66 = new ArrayList<>();
        learningOutcomes66.add("Die wesentlichen Unterschiede zwischen Qualitätssicherung, Qualitätskontrolle und Inspektionssystemen sowie deren Anwendung in der schweißtechnischen Fertigung beschreiben können.");
        learningOutcomes66.add("Schriftliche Qualitätskontrolleverfahren/Qualitätsanweisungen und -pläne darlegen können.");
        learningOutcomes66.add("Die einschlägigen Normen (z.B. ISO 9000 und ISO 3834) korrekt anwenden können.");
        learningOutcomes66.add("Grundlegende, die Qualität einer Schweißfertigung beeinflussende Faktoren wie Personal und Ausrüstung erklären können.");
        learningOutcomes66.add("Die Aufgaben des Schweißfachmannes in der industriellen Fertigung umreißen können");
        try {
            escoSkills66.add(new EscoSkill(new URL("http://data.europa.eu/esco/skill/c7cc6dc5-d56b-4323-8e5c-47c4022f615f")));
            escoSkills66.add(new EscoSkill(new URL("http://data.europa.eu/esco/skill/0415142d-6921-4f8d-832b-6271382b8193")));
        } catch (MalformedURLException e) {
            Log.warn("Wrong ESCO URL", e);
        }
        topic66.setLearningOutcomes(learningOutcomes66);
        topic66.setEscoSkills(escoSkills66);
        topics.add(topic66);

        // 67 Qualitätskontrolle während der Fertigung
        Topic topic67 = new Topic();
        topic67.setTitle("Qualitätskontrolle während der Fertigung");
        List<String> learningOutcomes67 = new ArrayList<>();
        List<EscoSkill> escoSkills67 = new ArrayList<>();
        learningOutcomes67.add("Den wesentlichen Zweck einer WPS/WPQR/pWPS sowie die Hauptvorteile geschweißter Konstruktionen in qualitativer Hinsicht erklären können.");
        learningOutcomes67.add("WPSs für Schweißkonstruktionen/-komponenten in Übereinstimmung mit nationalen und internationalen Normen erarbeiten und nachprüfen können.");
        learningOutcomes67.add("Die Normen zur Qualifizierung einer WPS korrekt anwenden und die wesentlichen Kennwerte für eine bestimmte WPS-Qualifizierung sowie ihren Geltungsbereich in Übereinstimmung mit nationalen und internati- onalen Normen bestimmen können.");
        learningOutcomes67.add("Den Hauptzweck einer Schweißerprüfung und die hauptsächlichen Qualitätsvorteile einer schweißtechni- schen Fertigung erläutern können.");
        learningOutcomes67.add("Die Schweißerprüfungsnorm korrekt anwenden sowie die wesentlichen Kennwerte für eine spezielle Schweißerprüfung und ihren Geltungsbereich darlegen können.");
        learningOutcomes67.add("Den wesentlichen Zweck der Prüfung von Bedienern von Schweißeinrichtungen und die hauptsächlichen Qualitätsvorteile einer schweißtechnischen Fertigung erklären können.");
        learningOutcomes67.add("Die Norm zur Prüfung von Bedienern von Schweißeinrichtungen korrekt anwenden und die wesentlichen Kennwerte einer bestimmten schweißtechnischen Bedienerprüfung sowie ihren Geltungsbereich darlegen können");
        learningOutcomes67.add("Die Anforderungen an die Rückverfolgbarkeit von Werkstoffverfahren und Zertifikaten umreißen und entsprechende Beispiele angeben können.");
        try {
            escoSkills67.add(new EscoSkill(new URL("http://data.europa.eu/esco/skill/2879a6f9-868b-40a1-a424-d50007ca8354")));
            escoSkills67.add(new EscoSkill(new URL("http://data.europa.eu/esco/skill/c7cc6dc5-d56b-4323-8e5c-47c4022f615f")));
        } catch (MalformedURLException e) {
            Log.warn("Wrong ESCO URL", e);
        }
        topic67.setLearningOutcomes(learningOutcomes67);
        topic67.setEscoSkills(escoSkills67);
        topics.add(topic67);

        // 68 Eigenspannungen und Verzug
        Topic topic68 = new Topic();
        topic68.setTitle("Eigenspannungen und Verzug");
        List<String> learningOutcomes68 = new ArrayList<>();
        List<EscoSkill> escoSkills68 = new ArrayList<>();
        learningOutcomes68.add("Den Ursprung, die Einflussfaktoren sowie die Höhe von Eigenspannungen und Verzug in geschweißten Konstruktionen umreißen können.");
        learningOutcomes68.add("Den Zusammenhang zwischen dem Werkstoff bei einer bestimmten Temperatur und seinen mechanischen Eigenschaften beschreiben können.");
        learningOutcomes68.add("Die Verteilung von Eigenspannungen in einer Schweißnaht (parallel zur Nahtachse, rechtwinklig und in Dickenrichtung, Einfluss der Werkstoffdicke) darstellen können.");
        learningOutcomes68.add("Die Schrumpfung und den Verzug in Schweißnähten und geschweißten Bauteilen nachvollziehen können.");
        learningOutcomes68.add("Die richtige Anwendung von Maßnahmen zur Minimierung des Verzuges und der Spannung darlegen können.");
        learningOutcomes68.add("Beschreiben können, wie Eigenspannungen das Verhalten eines Bauteils im Betrieb beeinflussen.");
        try {
            escoSkills68.add(new EscoSkill(new URL("http://data.europa.eu/esco/skill/1aba7c4a-4e13-4fa1-905b-7ed26952fbd5")));
        } catch (MalformedURLException e) {
            Log.warn("Wrong ESCO URL", e);
        }
        topic68.setLearningOutcomes(learningOutcomes68);
        topic68.setEscoSkills(escoSkills68);
        topics.add(topic68);


        // 69 Werkstatteinrichtungen, Schweißspann- und Haltevorrichtungen
        Topic topic69 = new Topic();
        topic69.setTitle("Werkstatteinrichtungen, Schweißspann- und Haltevorrichtungen");
        List<String> learningOutcomes69 = new ArrayList<>();
        List<EscoSkill> escoSkills69 = new ArrayList<>();
        learningOutcomes69.add("Die im Hinblick auf gesteigerte Produktivität, Sicherheit und Bequemlichkeit zu beachtenden Grundsätze umreißen können.");
        learningOutcomes69.add("Die Vorteile des Gebrauchs von Vorrichtungen, Spannvorrichtungen und Positioniereinrichtungen erklären können.");
        learningOutcomes69.add("Die für eine bestimmte Schweißkonstruktion zu verwendenden Arten von Vorrichtungen, Spannvorrichtungen und Positioniereinrichtungen bestimmen können.");
        learningOutcomes69.add("Die in bestimmten Bereichen der schweißtechnischen Fertigung zu verwendenden Hilfseinrichtungen sowie Kabel und Einrichtungen zur Wärmebehandlung und Temperaturüberwachung bestimmen können.");
        learningOutcomes69.add("Die speziellen Anforderungen bezüglich Schweißnahtvorbereitung und Heftschweißen erläutern können.");
        try {
            escoSkills69.add(new EscoSkill(new URL("http://data.europa.eu/esco/skill/5133ed7b-0785-4c28-825d-1dcf518c4b5b")));
            escoSkills69.add(new EscoSkill(new URL("http://data.europa.eu/esco/skill/98d0c976-2966-4f72-831b-ffd1f531a2c6")));
        } catch (MalformedURLException e) {
            Log.warn("Wrong ESCO URL", e);
        }
        topic69.setLearningOutcomes(learningOutcomes69);
        topic69.setEscoSkills(escoSkills69);
        topics.add(topic69);

        // 70 Gesundheitsschutz und Arbeitssicherheit
        Topic topic70 = new Topic();
        topic70.setTitle("Gesundheitsschutz und Arbeitssicherheit");
        List<String> learningOutcomes70 = new ArrayList<>();
        List<EscoSkill> escoSkills70 = new ArrayList<>();
        learningOutcomes70.add("Die durch elektrischen Strom, Gase, Rauche, Flammen, Strahlung und Lärm in Verbindung mit dem Schweißen verursachten Gesundheits- und Sicherheitsrisiken erklären können.");
        learningOutcomes70.add("Die mit den genannten Risiken verbundenen Gesundheits- und Arbeitsschutzbestimmungen korrekt auslegen können.");
        learningOutcomes70.add("Geeignete Maßnahmen zur Verringerung sämtlicher mit dem Schweißen verbundenen Risikofaktoren aufzeigen können.");
        learningOutcomes70.add("Die gesundheitlichen Auswirkungen des Schleifens und die mit Hitze verbundenen Risken angeben können.");
        learningOutcomes70.add("Den Einsatz sicherer Arbeitsabläufe demonstrieren können, die den sicherheitstechnischen Anforderungen entsprechen.");
        learningOutcomes70.add("Den korrekten Einsatz der Schutzkleidung genau beschreiben können.");
        learningOutcomes70.add("Die Sauerstoffanreicherung der Umgebung umreißen können.");
        try {
            escoSkills70.add(new EscoSkill(new URL("http://data.europa.eu/esco/skill/f2f463e5-2382-49cb-8199-e7e043d868df")));
        } catch (MalformedURLException e) {
            Log.warn("Wrong ESCO URL", e);
        }
        topic70.setLearningOutcomes(learningOutcomes70);
        topic70.setEscoSkills(escoSkills70);
        topics.add(topic70);

        // 71 Messen, Kontrollieren und Aufzeichnen von Schweißdaten
        Topic topic71 = new Topic();
        topic71.setTitle("Messen, Kontrollieren und Aufzeichnen von Schweißdaten");
        List<String> learningOutcomes71 = new ArrayList<>();
        List<EscoSkill> escoSkills71 = new ArrayList<>();
        learningOutcomes71.add("Die zur Überwachung des Schweißens und verwandter Verfahren eingesetzten Messmethoden umreißen können.");
        learningOutcomes71.add("Den richtigen Einsatz der Arbeitsabläufe für die Messung von Schweißparametern demonstrieren können.");
        learningOutcomes71.add("Den richtigen Einsatz der Arbeitsabläufe für die Messung und Überwachung von Wärmebehandlun- gen demonstrieren können.");
        try {
            escoSkills71.add(new EscoSkill(new URL("http://data.europa.eu/esco/skill/2879a6f9-868b-40a1-a424-d50007ca8354")));
        } catch (MalformedURLException e) {
            Log.warn("Wrong ESCO URL", e);
        }
        topic71.setLearningOutcomes(learningOutcomes71);
        topic71.setEscoSkills(escoSkills71);
        topics.add(topic71);


        // 72 Schweißnahtunregelmäßigkeiten
        Topic topic72 = new Topic();
        topic72.setTitle("Schweißnahtunregelmäßigkeiten ");
        List<String> learningOutcomes72 = new ArrayList<>();
        List<EscoSkill> escoSkills72 = new ArrayList<>();
        learningOutcomes72.add("Die grundlegenden Abnahmekriterien umreißen können.");
        learningOutcomes72.add("Den Einfluss der Größe, Gestalt und Position von Schweißnahtunregelmäßigkeiten auf die Widerstandsfähigkeit der Schweißkonstruktion erläutern können.");
        try {
            escoSkills72.add(new EscoSkill(new URL("http://data.europa.eu/esco/skill/371d57e6-cc5f-4fc0-9c0c-58feb595cc5b")));
            escoSkills72.add(new EscoSkill(new URL("http://data.europa.eu/esco/skill/75697ed4-c1a3-47dc-93e3-bb50f21aa2f6")));
        } catch (MalformedURLException e) {
            Log.warn("Wrong ESCO URL", e);
        }
        topic72.setLearningOutcomes(learningOutcomes72);
        topic72.setEscoSkills(escoSkills72);
        topics.add(topic72);

        // 73 Zerstörungsfreie Werkstoffprüfung
        Topic topic73 = new Topic();
        topic73.setTitle("Zerstörungsfreie Werkstoffprüfung");
        List<String> learningOutcomes73 = new ArrayList<>();
        List<EscoSkill> escoSkills73 = new ArrayList<>();
        learningOutcomes73.add("Die Arbeitsweise der grundlegenden, in der schweißtechnischen Fertigung angewandten ZfP Verfahren sowie ihre Vor- und Nachteile umreißen können.");
        learningOutcomes73.add("Schweißnahtunregelmäßigkeiten, ihre Ursachen und Vermeidung sowie Nachweismethoden beschreiben können.");
        learningOutcomes73.add("Die korrekte Anwendung der Bewertungsnormen für Schweißnahtunregelmäßigkeiten demonstrieren oder darstellen oder definieren können.");
        learningOutcomes73.add("Die Grundsätze der ZfP-Auswertung interpretieren können.");
        learningOutcomes73.add("Die ZfP- gerechte Schweißnahtanordnung und -gestaltung auswählen können.");
        learningOutcomes73.add("Die Ausbildung von ZfP-Personal klassifizieren können.");
        learningOutcomes73.add("Einschlägige Sicherheitsaspekte erkennen können.");
        try {
            escoSkills73.add(new EscoSkill(new URL("http://data.europa.eu/esco/skill/75697ed4-c1a3-47dc-93e3-bb50f21aa2f6")));
        } catch (MalformedURLException e) {
            Log.warn("Wrong ESCO URL", e);
        }
        topic73.setLearningOutcomes(learningOutcomes73);
        topic73.setEscoSkills(escoSkills73);
        topics.add(topic73);

        // 74 Wirtschaftlichkeit und Produktivität
        Topic topic74 = new Topic();
        topic74.setTitle("Wirtschaftlichkeit und Produktivität");
        List<String> learningOutcomes74 = new ArrayList<>();
        List<EscoSkill> escoSkills74 = new ArrayList<>();
        learningOutcomes74.add("Die mit Schweißarbeiten verbundenen Kosten bestimmen können.");
        learningOutcomes74.add("Die Kosten von Schweißarbeiten berechnen können.");
        learningOutcomes74.add("Schweiß- und Bearbeitungsverfahren einschließlich Mechanisierung und Automatisierung zur Minimierung der Fertigungskosten umreißen können.");
        try {
            escoSkills74.add(new EscoSkill(new URL("http://data.europa.eu/esco/skill/d58a48d1-5b7f-4028-ae11-b051d13ee1a8")));
            escoSkills74.add(new EscoSkill(new URL("http://data.europa.eu/esco/skill/6f8f86f8-0ef9-4dd6-8e29-7d790e65706b")));
        } catch (MalformedURLException e) {
            Log.warn("Wrong ESCO URL", e);
        }
        topic74.setLearningOutcomes(learningOutcomes74);
        topic74.setEscoSkills(escoSkills74);
        topics.add(topic74);

        // 75 Reparaturschweißen
        Topic topic75 = new Topic();
        topic75.setTitle("Reparaturschweißen");
        List<String> learningOutcomes75 = new ArrayList<>();
        List<EscoSkill> escoSkills75 = new ArrayList<>();
        learningOutcomes75.add("Die mit Reparaturschweißungen verbundenen Probleme umreißen können.");
        learningOutcomes75.add("Möglicherweise auftretende Gefährdungen im Zusammenhang mit Reparaturschweißungen  insbesondere im betrieblichen Einsatz erkennen können.");
        learningOutcomes75.add("Die Anwendung umfassender Verfahren in Bezug auf das Reparaturschweißen demonstrieren können.");
        learningOutcomes75.add("Die im Hinblick auf Reparaturschweißungen zu beachtenden personellen und verfahrenstechnischen  Anforderungen nennen können.");
        try {
            escoSkills75.add(new EscoSkill(new URL("http://data.europa.eu/esco/skill/6669458f-8692-4c7c-a1f0-bc9cc5310faa")));
        } catch (MalformedURLException e) {
            Log.warn("Wrong ESCO URL", e);
        }
        topic75.setLearningOutcomes(learningOutcomes75);
        topic75.setEscoSkills(escoSkills75);
        topics.add(topic75);

        // 76 Schweißen von Betonstahl
        Topic topic76 = new Topic();
        topic76.setTitle("Schweißen von Betonstahl");
        List<String> learningOutcomes76 = new ArrayList<>();
        List<EscoSkill> escoSkills76 = new ArrayList<>();
        learningOutcomes76.add("Die Grundlagen der verschiedenen Verbindungen umreißen können.");
        learningOutcomes76.add("Tragende und nicht tragende Verbindungen erkennen können.");
        learningOutcomes76.add("Geeignete Schweißprozesse klassifizieren können.");
        learningOutcomes76.add("Maßnahmen zur Bestimmung der Schweißnahtlänge in Abhängigkeit von ihrem Durchmesser erläutern können.");
        learningOutcomes76.add("Die Anwendung des Vorwärmens beschreiben können.");
        try {
            escoSkills76.add(new EscoSkill(new URL("http://data.europa.eu/esco/skill/2d0775c7-2dad-41fa-b8f3-988f73eb6897")));
        } catch (MalformedURLException e) {
            Log.warn("Wrong ESCO URL", e);
        }
        topic76.setLearningOutcomes(learningOutcomes76);
        topic76.setEscoSkills(escoSkills76);
        topics.add(topic76);

        courseHwkWelding.setTopics(topics);
        // visual representation
        try {
            ClassLoader classloader = Thread.currentThread().getContextClassLoader();
            InputStream is = classloader.getResourceAsStream("course/hwk/urn:myedulife:training:1:visual_template.svg");
            String data = readFromInputStream(is);
            courseHwkWelding.setVisualRepresentationTemplate(data);
        } catch (Exception e) {
            Log.warn("Cannot open file.", e);
        }
        coursesHwk.add(courseHwkWelding);
        return coursesHwk;
    }

    private static String readFromInputStream(InputStream inputStream)
            throws IOException {
        StringBuilder resultStringBuilder = new StringBuilder();
        try (BufferedReader br = new BufferedReader(new InputStreamReader(inputStream))) {
            String line;
            while ((line = br.readLine()) != null) {
                resultStringBuilder.append(line).append("\n");
            }
        }
        return resultStringBuilder.toString();
    }
}
