package course;

import rest.model.Topic;

import java.util.ArrayList;
import java.util.List;

public class DaoManager {

    private Dao<Course> courseDao = new CourseDao();
    private static final DaoManager instance = new DaoManager();

    //private constructor to avoid client applications to use constructor
    private DaoManager(){}

    public static DaoManager getInstance(){
        return instance;
    }

    public List<Topic> getTopicsFrom(CourseDao.INSTITUTION institution, String courseName) {
        for (Course course : this.courseDao.getAllFrom(institution)) {
            if(courseName.toLowerCase().trim().equals(course.getTitle().toLowerCase().trim())) {
               return course.getTopics();
            }
        }
        return new ArrayList<>();
    }

    public String getVisualRepresentationTemplateFrom(CourseDao.INSTITUTION institution, String courseName) {
        for (Course course : this.courseDao.getAllFrom(institution)) {
            if(courseName.toLowerCase().trim().equals(course.getTitle().toLowerCase().trim())) {
                return course.getVisualRepresentationTemplate();
            }
        }
        return "";
    }
}
