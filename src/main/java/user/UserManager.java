package user;

import org.eclipse.microprofile.config.inject.ConfigProperty;
import org.keycloak.OAuth2Constants;
import org.keycloak.admin.client.Keycloak;
import org.keycloak.admin.client.KeycloakBuilder;
import org.keycloak.representations.idm.CredentialRepresentation;
import org.keycloak.representations.idm.UserRepresentation;

import javax.enterprise.context.ApplicationScoped;
import javax.ws.rs.core.Response;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@ApplicationScoped
public class UserManager {

    @ConfigProperty(name = "keycloak.url", defaultValue="http://localhost:8080/auth")
    String keycloakServerUrl;

    @ConfigProperty(name = "keycloak.secret", defaultValue="EXqflX12n5HFhAQ9FFO2xGfVqlfOxAIX")
    String keycloakClientSecret;

    public Optional<String> getUserId(String lastName) {
        Keycloak keycloak = KeycloakBuilder.builder()
                .serverUrl(keycloakServerUrl)
                .realm("master")
                .clientId("admin-cli")
                .grantType(OAuth2Constants.CLIENT_CREDENTIALS)
                .clientSecret(keycloakClientSecret)
                .build();
        List<UserRepresentation> users = keycloak.realm("control-center").users().searchByAttributes("lastName:" + lastName + "");
        keycloak.close();
        if (users.isEmpty()) {
            return Optional.empty();
        } else {
            return Optional.of(users.get(0).getId());
        }

    }

    public Optional<Response> createUser(String nickname, String email, String firstName, String lastName, String password) {
        Keycloak keycloak = KeycloakBuilder.builder()
                .serverUrl(keycloakServerUrl)
                .realm("master")
                .clientId("admin-cli")
                .grantType(OAuth2Constants.CLIENT_CREDENTIALS)
                .clientSecret(keycloakClientSecret)
                .build();

        List<UserRepresentation> user = keycloak.realm("control-center").users().search(nickname);
        if (user.isEmpty()) {
            // create user
            UserRepresentation userRepresentation = new UserRepresentation();
            userRepresentation.setUsername(nickname);
            userRepresentation.setEmail(email);
            userRepresentation.setFirstName(firstName);
            userRepresentation.setLastName(lastName);
            CredentialRepresentation credentialRepresentation = new CredentialRepresentation();
            credentialRepresentation.setType(CredentialRepresentation.PASSWORD);
            credentialRepresentation.setValue(password);
            credentialRepresentation.setTemporary(Boolean.FALSE);
            List<CredentialRepresentation> credentialRepresentationList = new ArrayList<>();
            credentialRepresentationList.add(credentialRepresentation);
            userRepresentation.setCredentials(credentialRepresentationList);
            Optional<Response> response = Optional.of(keycloak.realm("control-center").users().create(userRepresentation));
            keycloak.close();
            return response;
        }
        keycloak.close();
        return Optional.empty();
    }
}
