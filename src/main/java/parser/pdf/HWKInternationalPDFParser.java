package parser.pdf;

import course.CourseDao;
import course.DaoManager;
import org.apache.pdfbox.pdmodel.PDDocument;
import org.apache.pdfbox.text.PDFTextStripper;
import org.jboss.logging.Logger;
import parser.CertificateParser;
import parser.vc.issuer.HWKIssuer;
import parser.vc.issuer.Issuer;
import rest.model.GenericCertificate;
import user.UserManager;

import java.io.File;
import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class HWKInternationalPDFParser implements CertificateParser {

    private static final Logger LOG = Logger.getLogger(HWKInternationalPDFParser.class);

    public HWKInternationalPDFParser() {}

    @Override
    public void parse(File uploadedFile, UserManager userManager, String keyStoreFolder, String vcFolder) {
        try {
            PDDocument doc = PDDocument.load(uploadedFile);
            String text = new PDFTextStripper().getText(doc);
            doc.close();
            LOG.info(text);
            List<GenericCertificate> genericCertificateList = new ArrayList<>();
            // create
            GenericCertificate genericCertificate = new GenericCertificate();
            // split to personal data and diploma data
            String[] separateContent = text.split("is hereby awarded the diploma of", 2);
            // name birthday title
            Pattern patternNBT = Pattern.compile("Nominated\\sBody\\n(.*)\\nDate of birth: (.*)\\n(.*)", Pattern.MULTILINE);
            Matcher matcherNBT = patternNBT.matcher(separateContent[0]);
            while (matcherNBT.find()) {
                if (matcherNBT.groupCount() == 3) {
                    String fullName = matcherNBT.group(1).trim();
                    String birthday = matcherNBT.group(2).trim();
                    String courseName = matcherNBT.group(3).trim();
                    // name
                    String[] names = fullName.split(" ");
                    String lastname = names[names.length - 1];
                    StringBuilder givenNames = new StringBuilder();
                    for (int i = 0; i < names.length - 1; i++) {
                        givenNames.append(names[i].trim()).append(" ");
                    }
                    genericCertificate.setGivenName(givenNames.toString().trim());
                    genericCertificate.setFamilyName(lastname.trim());
                    // birthday
                    genericCertificate.setBirthday(birthday);
                    // courseName
                    genericCertificate.setCourseName(courseName);
                }
            }
            // issuance date and certificate number
            Pattern patternIssuanceDate = Pattern.compile("Date:\\s(.*)\\s{2}Diploma No:\\s(.*)\\n", Pattern.MULTILINE);
            Matcher matcherIssuanceDate = patternIssuanceDate.matcher(separateContent[1]);
            while (matcherIssuanceDate.find()) {
                if (matcherIssuanceDate.groupCount() == 2) {
                    String issuanceDate = matcherIssuanceDate.group(1).trim();
                    String certificateNumber = matcherIssuanceDate.group(2).trim();
                    // issuance date
                    genericCertificate.setIssuanceDate(issuanceDate);
                    // certificate number
                    genericCertificate.setCertificateNumber(certificateNumber);
                }
            }
            // place
            genericCertificate.setPlace("Dresden");
            // topics
            genericCertificate.setTopics(DaoManager.getInstance().getTopicsFrom(CourseDao.INSTITUTION.HWK, genericCertificate.getCourseName()));
            // add certificate to list
            genericCertificateList.add(genericCertificate);
            LOG.info(genericCertificate.toString());
            // generate VCs
            Issuer issuer = new HWKIssuer();
            generateVerifiableCredentials(genericCertificateList, userManager, keyStoreFolder, vcFolder, issuer);
        } catch (Exception e) {
            LOG.error("Cannot parse or close pdf file.", e);
        }
    }

    @Override
    public boolean validate(File uploadedFile) {
        // try to load the PDF file and check whether the file is encrypted
        try {
            PDDocument doc = PDDocument.load(uploadedFile);
            // check encryption
            if (doc.isEncrypted()) {
                doc.close();
                return false;
            }
            // check String IIW...
            String text = new PDFTextStripper().getText(doc);
            if (!text.contains("IIW Authorised Nominated Body")) {
                doc.close();
                return false;
            }
            doc.close();
        } catch (Exception e) {
            LOG.info("Uploaded file is not valid.");
            return false;
        }
        return true;
    }
}
