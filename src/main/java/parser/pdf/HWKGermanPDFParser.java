package parser.pdf;

import org.apache.pdfbox.pdmodel.PDDocument;
import org.apache.pdfbox.text.PDFTextStripper;
import org.jboss.logging.Logger;
import parser.CertificateParser;
import user.UserManager;

import java.io.File;

public class HWKGermanPDFParser implements CertificateParser {

    private static final Logger LOG = Logger.getLogger(HWKGermanPDFParser.class);

    public HWKGermanPDFParser() {}

    @Override
    public void parse(File uploadedFile, UserManager userManager, String keyStoreFolder, String vcFolder) {

        try {
            PDDocument doc = PDDocument.load(uploadedFile);
            String text = new PDFTextStripper().getText(doc);
            doc.close();
            LOG.info(text);
        } catch (Exception e) {
            LOG.error("Cannot parse pdf file.", e);
        }
    }

    @Override
    public boolean validate(File uploadedFile) {
        // try to load the PDF file and check whether the file is encrypted
        try {
            PDDocument doc = PDDocument.load(uploadedFile);
            // check encryption
            if (doc.isEncrypted()) {
                doc.close();
                return false;
            }
            // check String Handwerkskammer...
            String text = new PDFTextStripper().getText(doc);
            if (!text.contains("der Handwerkskammer Dresden")) {
                doc.close();
                return false;
            }
            doc.close();
        } catch (Exception e) {
            LOG.info("Uploaded file is not valid.");
            return false;
        }
        return true;
    }
}
