@XmlSchema(
        namespace = "http://schemas.openxmlformats.org/wordprocessingml/2006/main",
        elementFormDefault = XmlNsForm.QUALIFIED,
        xmlns = {
                @XmlNs(prefix = "w", namespaceURI = "http://schemas.openxmlformats.org/wordprocessingml/2006/main"),

        }
)

package parser.docx.model;

import javax.xml.bind.annotation.XmlNs;
import javax.xml.bind.annotation.XmlNsForm;
import javax.xml.bind.annotation.XmlSchema;