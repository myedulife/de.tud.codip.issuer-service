package parser.docx;

import course.CourseDao;
import course.DaoManager;
import org.apache.poi.xwpf.extractor.XWPFWordExtractor;
import org.apache.poi.xwpf.usermodel.XWPFDocument;
import org.jboss.logging.Logger;
import parser.CertificateParser;
import parser.vc.issuer.EBZIssuer;
import parser.vc.issuer.Issuer;
import rest.model.GenericCertificate;
import user.UserManager;

import java.io.File;
import java.io.FileInputStream;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class EBZDocxParser implements CertificateParser {

    private static final Logger LOG = Logger.getLogger(EBZDocxParser.class);

    public EBZDocxParser() {}

    @Override
    public void parse(File uploadedFile, UserManager userManager, String keyStoreFolder, String vcFolder) {
        try {
            FileInputStream fip = new FileInputStream(uploadedFile);
            XWPFDocument doc = new XWPFDocument(fip);
            XWPFWordExtractor extractor = new XWPFWordExtractor(doc);
            String text = extractor.getText();
            LOG.info(text);
            // split certificates
            String[] certificates = text.split("Seminarleiter");
            List<GenericCertificate> genericCertificateList = new ArrayList<>();
            for (String certificate : certificates) {
                if (certificate.trim().length() > 0) {
                    // create new object
                    GenericCertificate genericCertificate = new GenericCertificate();
                    // gender, givenname, and familyname
                    String genderFullName = certificate.split("geboren am")[0].trim();
                    String[] genderFullNameArray = genderFullName.split(" ");
                    if (genderFullNameArray.length > 2) {
                        String gender = genderFullNameArray[0].trim();
                        String familyName = genderFullNameArray[genderFullNameArray.length - 1].trim().replaceAll(",", "");
                        StringBuilder givenNames = new StringBuilder();
                        for (int i = 1; i < genderFullNameArray.length - 1; i++) {
                            givenNames.append(genderFullNameArray[i].trim()).append(" ");
                        }
                        genericCertificate.setGender(gender);
                        genericCertificate.setFamilyName(familyName);
                        genericCertificate.setGivenName(givenNames.toString().trim());
                    }
                    // birthday
                    String birthday = certificate.split("hat am")[0];
                    String[] birthdayDraft = birthday.split("geboren am ");
                    int length = birthdayDraft.length;
                    birthday = birthdayDraft[length - 1].trim();
                    genericCertificate.setBirthday(birthday);
                    // course title, start- and end date
                    String courseNameStartEndDate = certificate.split("mit Erfolg teilgenommen.")[0].trim();
                    String[] courseNameStartEndDateArray = courseNameStartEndDate.split("hat am Lehrgang", 2);
                    String line = courseNameStartEndDateArray[1];

                    Pattern pattern = Pattern.compile("(\\n+)(.*)(\\n+)vom\\s(.*)\\sbis\\s(.*)", Pattern.MULTILINE);
                    Matcher matcher = pattern.matcher(line);
                    while (matcher.find()) {
                        if (matcher.groupCount() == 5) {
                            String courseName = matcher.group(2).trim();
                            DateTimeFormatter formatter = DateTimeFormatter.ofPattern("dd.MM.yyyy", Locale.GERMAN);
                            LocalDate start = LocalDate.parse(matcher.group(4).trim(), formatter);
                            LocalDate end = LocalDate.parse(matcher.group(5).trim(), formatter);
                            genericCertificate.setCourseName(courseName);
                            genericCertificate.setStart(start);
                            genericCertificate.setEnd(end);
                        }
                    }
                    // hours
                    String hoursPart = certificate.split("Unterrichtsstunden")[0];
                    String[] hoursDraft = hoursPart.split("Umfang:", 2);
                    Double hoursD = Double.parseDouble(hoursDraft[1].trim());
                    genericCertificate.setHours(hoursD.intValue());
                    // place
                    genericCertificate.setPlace("Dresden");
                    // topics
                    genericCertificate.setTopics(DaoManager.getInstance().getTopicsFrom(CourseDao.INSTITUTION.EBZ, genericCertificate.getCourseName()));
                    // managing director and seminar leader
                    Pattern patternMDandSL = Pattern.compile("^(.*)Geschäftsführer(.*)$", Pattern.MULTILINE);
                    Matcher matcherMDandSL = patternMDandSL.matcher(certificate);
                    while (matcherMDandSL.find()) {
                        if (matcherMDandSL.groupCount() == 2) {
                            String managingDirector = matcherMDandSL.group(1).trim();
                            String seminarLeader = matcherMDandSL.group(2).trim();
                            genericCertificate.setManagingDirector(managingDirector);
                            genericCertificate.setSeminarLeader(seminarLeader);
                        }
                    }
                    // add generic certificate
                    genericCertificateList.add(genericCertificate);
                    LOG.info(genericCertificate.toString());
                }
            }
            // generate VCs
            Issuer issuer = new EBZIssuer();
            generateVerifiableCredentials(genericCertificateList, userManager, keyStoreFolder, vcFolder, issuer);
        } catch (Exception e) {
            LOG.error("Processing failed.", e);
        }
    }

    @Override
    public boolean validate(File uploadedFile) {
        try {
            FileInputStream fip = new FileInputStream(uploadedFile);
            XWPFDocument doc = new XWPFDocument(fip);
            XWPFWordExtractor extractor = new XWPFWordExtractor(doc);
            String text = extractor.getText();
            if (text.contains("Jens Köster")) {
                return true;
            }
        } catch (Exception e) {
            LOG.info("Uploaded file is not valid.");
        }
        return false;
    }
}
