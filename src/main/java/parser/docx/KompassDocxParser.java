package parser.docx;

import org.jboss.logging.Logger;
import parser.CertificateParser;
import parser.docx.docxunzipper.DocxJM;
import parser.docx.docxunzipper.Helper;
import parser.docx.model.DJMDocument;
import parser.vc.issuer.Issuer;
import parser.vc.issuer.KompassIssuer;
import rest.model.EscoSkill;
import rest.model.GenericCertificate;
import rest.model.Topic;
import user.UserManager;

import java.io.File;
import java.net.URL;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Locale;

public class KompassDocxParser implements CertificateParser {

    private static final Logger LOG = Logger.getLogger(KompassDocxParser.class);

    public KompassDocxParser() {}

    public void parse(File uploadedFile, UserManager userManager, String keyStoreFolder, String vcFolder) {
        try {
            DJMDocument djmDocument = DocxJM.map(uploadedFile.toString());
            String text = Helper.getTextFromDocument(djmDocument);
            LOG.info(text);
            // split zeugnisse
            String[] certificates = text.split("Zeugnis");
            List<GenericCertificate> genericCertificateList = new ArrayList<>();
            for (String certificate : certificates) {
                if (certificate.trim().length() > 0) {
                    GenericCertificate genericCertificate = new GenericCertificate();
                    // gender
                    String gender = certificate.split("null")[0];
                    genericCertificate.setGender(gender);
                    // name
                    String name = certificate.split("geboren am")[0].split("null")[1];
                    String[] names = name.split(",", 2);
                    genericCertificate.setFamilyName(names[0].trim());
                    genericCertificate.setGivenName(names[1].trim());
                    // birthday
                    String birthday = certificate.split("hat vom")[0];
                    String[] birthdayDraft = birthday.split("null");
                    int length = birthdayDraft.length;
                    birthday = birthdayDraft[length - 1];
                    genericCertificate.setBirthday(birthday);
                    // start, end
                    String startEnd = certificate.split("an der Fortbildung")[0];
                    String[] startEndDraft = startEnd.split("null");
                    length = startEndDraft.length;
                    startEnd = startEndDraft[length - 1];
                    startEndDraft = startEnd.split(" bis ");
                    DateTimeFormatter formatter = DateTimeFormatter.ofPattern("dd.MM.yyyy", Locale.GERMAN);
                    LocalDate start = LocalDate.parse(startEndDraft[0], formatter);
                    genericCertificate.setStart(start);
                    LocalDate end = LocalDate.parse(startEndDraft[1], formatter);
                    genericCertificate.setEnd(end);
                    // course name
                    String courseName = certificate.split("innull")[0];
                    String[] courseNameDraft = courseName.split("null");
                    length = courseNameDraft.length;
                    courseName = courseNameDraft[length - 1];
                    genericCertificate.setCourseName(courseName);
                    // place
                    String place = certificate.split(" teilgenommen.")[0];
                    String[] placeDraft = place.split("null");
                    length = placeDraft.length;
                    place = placeDraft[length - 1];
                    genericCertificate.setPlace(place);
                    // Hours
                    String hours = certificate.split(" Stunden wurden")[0];
                    String[] hoursDraft = hours.split(".In ");
                    length = hoursDraft.length;
                    hours = hoursDraft[length - 1];
                    genericCertificate.setHours(Integer.parseInt(hours));
                    // topics
                    String topics = certificate.split("Ulrike KühnelLeiterin")[0];
                    String[] topicsDraft = topics.split("nullNote");
                    length = topicsDraft.length;
                    topics = topicsDraft[length - 1];
                    String[] table = topics.split("h\\)\\s*null");
                    Topic[] topicArray = new Topic[table.length - 1];
                    int rowNumber = 0;
                    int maxRow = table.length - 1;
                    for (String row: table) {
                        if (rowNumber < maxRow) {
                            //System.out.println(row);
                            Topic topic = new Topic();
                            String[] titleHours = row.split(" \\(");
                            if (titleHours != null && titleHours.length > 1) {
                                String title = titleHours[0];
                                if (title.startsWith("m.E.t.")) {
                                    title = title.substring(6);
                                } else if (title.startsWith(" ")) {
                                    String[] markParts = title.split(",", 2);
                                    title = markParts[1].substring(1);
                                }
                                topic.setTitle(title);
                                String titleHoursNumber = titleHours[1].split("h\\)")[0];
                                topic.setHours(Integer.parseInt(titleHoursNumber));
                                //TODO:bja load from course data
                                topic.getEscoSkills().add(new EscoSkill(new URL("http://data.europa.eu/esco/skill/bdcf429c-5ccf-4c3d-bb61-4c987573a35e")));
                                topicArray[rowNumber] = topic;
                                String nextRow = table[rowNumber + 1];
                                if (nextRow != null) {
                                    if (nextRow.startsWith("m.E.t.")) {
                                        topic.setMark("m.E.t.");
                                    } else if (nextRow.startsWith(" ")) {
                                        String[] markParts = nextRow.split(",");
                                        String mark = markParts[0].trim() + "," + markParts[1].substring(0, 1);
                                        topic.setMark(mark);
                                    }
                                }
                            }
                            rowNumber++;
                        }
                    }
                    genericCertificate.setTopics(Arrays.asList(topicArray));
                    genericCertificateList.add(genericCertificate);
                    LOG.info(genericCertificate.toString());
                }
            }
            // generate VCs
            Issuer issuer = new KompassIssuer();
            generateVerifiableCredentials(genericCertificateList, userManager, keyStoreFolder, vcFolder, issuer);
        } catch (Exception e) {
            LOG.error("Processing failed.", e);
        }
    }

    @Override
    public boolean validate(File uploadedFile) {
        boolean isValid = false;
        try {
            DJMDocument djmDocument = DocxJM.map(uploadedFile.toString());
            String text = Helper.getTextFromDocument(djmDocument);
            // split zeugnisse
            String[] certificates = text.split("Zeugnis");
            String gender = "";
            String name = "";
            String birthday = "";
            for (String certificate : certificates) {
                if (certificate.trim().length() > 0) {
                    // gender
                    gender = certificate.split("null")[0];
                    // name
                    name = certificate.split("geboren am")[0].split("null")[1];
                    // birthday
                    birthday = certificate.split("hat vom")[0];
                }
            }
            if(gender.length() > 0 && name.length() > 0 && birthday.length() > 0) {
                isValid = true;
            }

        } catch (Exception e) {
            LOG.info("Uploaded file is not valid.");
        }

        return isValid;
    }

}
