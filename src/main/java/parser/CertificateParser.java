package parser;

import parser.vc.VCHelper;
import parser.vc.issuer.Issuer;
import rest.model.GenericCertificate;
import user.UserManager;

import java.io.File;
import java.util.List;

public interface CertificateParser {
    public void parse(File uploadedFile, UserManager userManager, String keyStoreFolder, String vcFolder);

    boolean validate(File uploadedFile);

    default void generateVerifiableCredentials(List<GenericCertificate> genericCertificateList, UserManager userManager, String keyStoreFolder, String vcFolder, Issuer issuer) {
        for (GenericCertificate genericCertificate : genericCertificateList) {
            VCHelper.generateVC(genericCertificate, issuer, userManager, keyStoreFolder, vcFolder);
        }
    }

}
