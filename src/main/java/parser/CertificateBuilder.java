package parser;

import parser.docx.EBZDocxParser;
import parser.docx.KompassDocxParser;
import parser.pdf.HWKGermanPDFParser;
import parser.pdf.HWKInternationalPDFParser;

import java.io.File;
import java.util.Optional;

public class CertificateBuilder {

    private CertificateParser kompassDocxParser = new KompassDocxParser();

    private CertificateParser ebzDocxParser = new EBZDocxParser();

    private CertificateParser hwkInternationalPdfParser = new HWKInternationalPDFParser();
    private CertificateParser hwkGermanPdfParser = new HWKGermanPDFParser();

    private static final CertificateBuilder instance = new CertificateBuilder();

    //private constructor to avoid client applications to use constructor
    private CertificateBuilder(){}

    public static CertificateBuilder getInstance(){
        return instance;
    }
    public Optional<CertificateParser> buildParser(File uploadedFile) {
        if (this.kompassDocxParser.validate(uploadedFile)) {
            // is kompass docx
            return Optional.of(kompassDocxParser);
        } else if (this.ebzDocxParser.validate(uploadedFile)) {
            // is ebz docx
            return Optional.of(ebzDocxParser);
        } else if (this.hwkInternationalPdfParser.validate(uploadedFile)) {
            // is hwk pdf
            return Optional.of(hwkInternationalPdfParser);
        } else if (this.hwkGermanPdfParser.validate(uploadedFile)) {
            // is hwk pdf
            return Optional.of(hwkGermanPdfParser);
        }
        return Optional.empty();
    }
}
