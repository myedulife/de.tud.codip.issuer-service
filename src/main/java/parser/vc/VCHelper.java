package parser.vc;

import com.fasterxml.jackson.databind.ObjectMapper;
import parser.vc.issuer.Issuer;
import parser.vc.model.VerifiableCredential;
import parser.vc.model.VerifiableCredentialWithoutProof;
import rest.model.GenericCertificate;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.security.*;
import java.security.cert.Certificate;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Base64;

import org.jboss.logging.Logger;
import user.UserManager;

public class VCHelper {

    private static final Logger LOG = Logger.getLogger(VCHelper.class);
    private static final int MIN = 1;
    private static final int MAX = 100000;

    public static void generateVC(GenericCertificate genericCertificate, Issuer issuer, UserManager userManager, String keyStoreFolder, String vcFolder) {
        try {
            VerifiableCredential vc = new VerifiableCredential(genericCertificate, issuer, userManager);
            ObjectMapper mapper = new ObjectMapper();
            KeyStore keyStore = KeyStore.getInstance("PKCS12");
            // TODO:bja change password
            keyStore.load(new FileInputStream(keyStoreFolder), "changeit".toCharArray());
            Certificate certificate = keyStore.getCertificate("senderKeyPair");
            PublicKey publicKey = certificate.getPublicKey();
            LOG.info("Pubkey: " + publicKey);
            PrivateKey privateKey = (PrivateKey) keyStore.getKey("senderKeyPair", "changeit".toCharArray());
            // sign
            Signature signature = Signature.getInstance("SHA256withRSA");
            signature.initSign(privateKey);
            // load content
            VerifiableCredentialWithoutProof vcWithoutProof = generateVcWithoutProof(vc);
            String vcSubject = mapper.writerWithDefaultPrettyPrinter().writeValueAsString(vcWithoutProof);
            signature.update(vcSubject.getBytes(StandardCharsets.UTF_8));
            byte[] digitalSignature = signature.sign();
            // set proof part
            vc.getProof().setSignatureValue(Base64.getEncoder().encodeToString(digitalSignature));
            vc.getProof().setType("SHA256withRSA");
            vc.getProof().setProofPurpose("assertionMethod");
            vc.getProof().setVerificationMethod("https://myedulife.de/issuers/key-1");
            LocalDateTime today = LocalDateTime.now();
            String formattedDate = today.format(DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss"));
            vc.getProof().setCreated(formattedDate);
            // write to file
            int x = MIN + (int)(Math.random() * ((MAX - MIN) + 1));
            String randomNumber = String.valueOf(x);
            Files.createDirectories(Paths.get(vcFolder));
            String fileName = vcFolder + "/vc_" + vc.getCredentialSubject().getId() + "_" + vc.getCredentialSubject().getAchievement().getId() + "_" + randomNumber + ".json";
            File uploadedFile = new File(fileName);
            if (!uploadedFile.exists()) {
                writeFile(mapper.writerWithDefaultPrettyPrinter().writeValueAsBytes(vc), fileName);
            }
            //LOG.info(mapper.writerWithDefaultPrettyPrinter().writeValueAsString(vc));
        } catch (Exception e) {
            LOG.error("Cannot generate vc.", e);
        }


    }

    private static VerifiableCredentialWithoutProof generateVcWithoutProof(VerifiableCredential vc) {
        VerifiableCredentialWithoutProof vcWithoutProof = new VerifiableCredentialWithoutProof();
        vcWithoutProof.setId(vc.getId());
        vcWithoutProof.setContext(vc.getContext());
        vcWithoutProof.setIssuer(vc.getIssuer());
        vcWithoutProof.setIssuerName(vc.getIssuerName());
        vcWithoutProof.setIssuanceDate(vc.getIssuanceDate());
        vcWithoutProof.setCredentialSubject(vc.getCredentialSubject());
        vcWithoutProof.setIssuerPublicKey(vc.getIssuerPublicKey());
        vcWithoutProof.setType(vc.getType());
        return vcWithoutProof;
    }

    private static void writeFile(byte[] content, String filename) throws IOException {
        File file = new File(filename);
        if (!file.exists()) {
            file.createNewFile();
        }
        FileOutputStream fop = new FileOutputStream(file);
        fop.write(content);
        fop.flush();
        fop.close();
    }
}
