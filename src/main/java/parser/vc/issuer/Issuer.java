package parser.vc.issuer;

import course.CourseDao;

public interface Issuer {
    String getDid();

    String getName();

    String getIssuerPublicKey();

    CourseDao.INSTITUTION getInstitution();
}
