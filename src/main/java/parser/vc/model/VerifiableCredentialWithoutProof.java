package parser.vc.model;

import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import lombok.Getter;
import lombok.Setter;

import java.util.ArrayList;
import java.util.List;

@Getter
@Setter
@JsonPropertyOrder({ "context", "id", "type", "issuer", "issuerName", "issuerPublicKey", "issuanceDate", "credentialSubject" })
public class VerifiableCredentialWithoutProof {
    List<String> context = new ArrayList<>();
    String id;
    List<String> type = new ArrayList<>();
    String issuer;
    String issuerName;
    String issuerPublicKey;
    String issuanceDate;
    CredentialSubject credentialSubject;
}
