package parser.vc.model;

import lombok.Getter;
import lombok.Setter;
import parser.vc.issuer.Issuer;

@Getter
@Setter
public class Proof {

    String type;
    String created;
    String proofPurpose;
    String verificationMethod;
    String signatureValue;

    public Proof(Issuer issuer) {

    }
}
