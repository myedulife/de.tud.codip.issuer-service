package parser.vc.model;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class Identifier {
    String schemeID;
    String value;

    public Identifier(String schemeID, String value) {
        this.schemeID = schemeID;
        this.value = value;
    }
}
