package parser.vc.model;

import lombok.Getter;
import lombok.Setter;
import rest.model.EscoSkill;
import rest.model.GenericCertificate;
import rest.model.Topic;

import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.List;

@Getter
@Setter
public class Achievement {

    String id;
    String title;
    String text;
    String start_date;
    String end_date;
    List<LearningActivity> learningActivities = new ArrayList<>();
    List<LearningOutcome> learningOutcomes = new ArrayList<>();
    List<LearningOutcomeEsco> learningOutcomesEsco = new ArrayList<>();

    public Achievement(GenericCertificate genericCertificate) {
        this.id = genericCertificate.getId();
        this.title = genericCertificate.getCourseName();
        this.text = genericCertificate.getCourseDescription();
        if (genericCertificate.getStart() != null) this.start_date = genericCertificate.getStart().format(DateTimeFormatter.ofPattern("yyyy-MM-dd"));
        if (genericCertificate.getEnd() != null) this.end_date = genericCertificate.getEnd().format(DateTimeFormatter.ofPattern("yyyy-MM-dd"));
        // learning activities
        int learningActivityId = 0;
        for(Topic topic : genericCertificate.getTopics()) {
            // activity
            String activityId = "urn:myedulife:activity:" + Integer.toString(learningActivityId);
            LearningActivity learningActivity = new LearningActivity(activityId, topic.getTitle(), topic.getHours(), topic.getMark());
            this.learningActivities.add(learningActivity);
            // outcomes
            for(String outcome : topic.getLearningOutcomes()) {
                LearningOutcome learningOutcome = new LearningOutcome(activityId, outcome);
                this.learningOutcomes.add(learningOutcome);
            }
            // esco outcomes
            for(EscoSkill skill : topic.getEscoSkills()) {
                LearningOutcomeEsco learningOutcomeEsco = new LearningOutcomeEsco(activityId, skill.getSkill());
                this.learningOutcomesEsco.add(learningOutcomeEsco);
            }
            learningActivityId++;
        }
    }
}
