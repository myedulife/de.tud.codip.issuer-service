package parser.vc.model;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class LearningActivity {

    String id;
    String title;
    String lecturer;
    String lecturerId;
    int hours;
    String mark;

    public LearningActivity(String id, String title, int hours, String mark) {
        this.id = id;
        this.title = title;
        this.hours = hours;
        this.mark = mark;
    }
}
