package parser.vc.model;

import com.fasterxml.jackson.annotation.JsonGetter;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import lombok.Getter;
import lombok.Setter;
import parser.vc.issuer.Issuer;
import rest.model.GenericCertificate;
import user.UserManager;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

@Getter
@Setter
@JsonPropertyOrder({ "context", "id", "type", "issuer", "issuerName", "issuerPublicKey", "issuanceDate", "credentialSubject", "proof" })
public class VerifiableCredential {
    List<String> context = new ArrayList<>();
    String id;
    List<String> type = new ArrayList<>();
    String issuer;
    String issuerName;
    String issuerPublicKey;
    String issuanceDate;
    CredentialSubject credentialSubject;
    Proof proof;

    public VerifiableCredential(GenericCertificate genericCertificate, Issuer issuer, UserManager userManager) throws Exception {
        context.add("https://www.w3.org/2018/credentials/v1");
        context.add("https://myedulife.de/credentialschema");
        this.id = "urn:myedulife:cred:" + UUID.randomUUID();
        type.add("VerifiableCredential");
        type.add("Europass");
        type.add(cleanTextContent(genericCertificate.getCourseName()));
        this.issuer = issuer.getDid();
        this.issuerName = issuer.getName();
        this.issuerPublicKey = issuer.getIssuerPublicKey();
        if (genericCertificate.getIssuanceDate().isEmpty()) {
            LocalDateTime today = LocalDateTime.now();
            String formattedDate = today.format(DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss"));
            this.issuanceDate = formattedDate;
        } else {
            this.issuanceDate = genericCertificate.getIssuanceDate();
        }
        this.credentialSubject = new CredentialSubject(genericCertificate, issuer, userManager);
        this.proof = new Proof(issuer);
    }

    @JsonGetter("@context")
    public List<String> getContext() {
        return context;
    }

    public void setContext(List<String> context) {
        this.context = context;
    }

    private String cleanTextContent(String text)
    {
        // strips off all non-ASCII characters
        String cleanedText = text.replaceAll("[^\\x00-\\x7F]", "");

        // erases all the ASCII control characters
        cleanedText = cleanedText.replaceAll("[\\p{Cntrl}&&[^\r\n\t]]", "");

        // removes non-printable characters from Unicode
        cleanedText = cleanedText.replaceAll("\\p{C}", "");

        // strips off all spaces
        cleanedText = cleanedText.replaceAll("\\s", "");

        // remove all punctuations
        cleanedText = cleanedText.replaceAll("\\p{Punct}", "");

        return cleanedText.trim();
    }

}
