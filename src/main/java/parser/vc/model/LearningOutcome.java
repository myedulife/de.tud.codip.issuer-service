package parser.vc.model;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class LearningOutcome {

    String id;
    String text;
    public LearningOutcome(String id, String title) {
        this.id = id;
        this.text = title;
    }
}
