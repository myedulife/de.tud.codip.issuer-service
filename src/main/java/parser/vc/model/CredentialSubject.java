package parser.vc.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import course.DaoManager;
import lombok.Getter;
import lombok.Setter;
import parser.vc.issuer.Issuer;
import rest.model.GenericCertificate;
import user.UserManager;

import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Getter
@Setter
@JsonIgnoreProperties({"userManager"})
public class CredentialSubject {
    String id;
    List<Identifier> identifiers = new ArrayList();
    String currentFamilyName;
    String currentGivenName;
    String dateOfBirth;
    Achievement achievement;
    String visualRepresentation;


    public CredentialSubject(GenericCertificate genericCertificate, Issuer issuer, UserManager userManager) throws Exception {
        Optional<String> userId = userManager.getUserId(genericCertificate.getFamilyName());
        if (userId.isEmpty()) {
            throw new Exception("Cannot found user with lastname " + genericCertificate.getFamilyName() + " in keycloak.");
        }
        String userKeyCloakId = userId.get();
        this.id = "did:myedulife:person:" + userKeyCloakId;
        identifiers.add(new Identifier("Student identification number", userKeyCloakId));
        this.currentFamilyName = genericCertificate.getFamilyName();
        this.currentGivenName = genericCertificate.getGivenName();
        this.dateOfBirth = genericCertificate.getBirthday();
        this.achievement = new Achievement(genericCertificate);
        String vrTemplate = DaoManager.getInstance().getVisualRepresentationTemplateFrom(issuer.getInstitution(), genericCertificate.getCourseName());
        // name
        vrTemplate = vrTemplate.replaceAll("TEMPLATE_SVG_NAME", normalizeForSVG(this.currentGivenName + " " + this.currentFamilyName));
        // birthday
        vrTemplate = vrTemplate.replaceAll("TEMPLATE_SVG_DATE_OF_BIRTH", this.dateOfBirth);
        // date
        vrTemplate = vrTemplate.replaceAll("TEMPLATE_SVG_DATE", genericCertificate.getIssuanceDate());
        // course name
        String courseName = genericCertificate.getCourseName();
        String[] courseNameWords = courseName.split(" ");
        String line1 = "";
        String line2 = "";
        if (courseNameWords.length > 4) {
            StringBuilder sb1 = new StringBuilder();
            for (int i = 0; i < 4; i++) {
                sb1.append(courseNameWords[i]).append(" ");
            }
            line1 = sb1.toString().trim();
            StringBuilder sb2 = new StringBuilder();
            for (int i = 4; i < courseNameWords.length; i++) {
                sb2.append(courseNameWords[i]).append(" ");
            }
            line2 = sb2.toString().trim();
        } else {
            line1 = courseName;
        }
        // course name line 1
        vrTemplate = vrTemplate.replaceAll("TEMPLATE_SVG_COURSE_NAME_LINE1", normalizeForSVG(line1));
        // course name line 2
        vrTemplate = vrTemplate.replaceAll("TEMPLATE_SVG_COURSE_NAME_LINE2", normalizeForSVG(line2));
        // diploma number
        vrTemplate = vrTemplate.replaceAll("TEMPLATE_SVG_DIPLOMA_NUMBER", genericCertificate.getCertificateNumber());
        // start date
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("dd.MM.yyyy");
        vrTemplate = vrTemplate.replaceAll("TEMPLATE_SVG_START_DATE", genericCertificate.getStart().format(formatter));
        // finish date
        vrTemplate = vrTemplate.replaceAll("TEMPLATE_SVG_FINISH_DATE", genericCertificate.getEnd().format(formatter));
        // hours
        vrTemplate = vrTemplate.replaceAll("TEMPLATE_SVG_HOURS", Integer.toString(genericCertificate.getHours()));
        // managing director
        vrTemplate = vrTemplate.replaceAll("TEMPLATE_SVG_MD", normalizeForSVG(genericCertificate.getManagingDirector()));
        // seminar leader
        vrTemplate = vrTemplate.replaceAll("TEMPLATE_SVG_SL", normalizeForSVG(genericCertificate.getSeminarLeader()));
        // remove new line
        vrTemplate = vrTemplate.replaceAll("\n", "");
        this.visualRepresentation = vrTemplate;
    }

    private String normalizeForSVG(String s) {
        if (s == null) return "";
        String normalizedString = s.replaceAll("Ä", "&#196;");
        normalizedString = normalizedString.replaceAll("ä", "&#228;");
        normalizedString = normalizedString.replaceAll("Ö", "&#214;");
        normalizedString = normalizedString.replaceAll("ö", "&#246;");
        normalizedString = normalizedString.replaceAll("Ü", "&#220;");
        normalizedString = normalizedString.replaceAll("ü", "&#252;");
        normalizedString = normalizedString.replaceAll("ß", "&#223;");
        normalizedString = normalizedString.replaceAll("§", "&#167;");
        normalizedString = normalizedString.replaceAll("®", "&#174;");
        normalizedString = normalizedString.replaceAll("©", "&#169;");
        return normalizedString;
    }

}
