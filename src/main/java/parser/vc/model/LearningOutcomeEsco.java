package parser.vc.model;

import lombok.Getter;
import lombok.Setter;

import java.net.URL;

@Getter
@Setter
public class LearningOutcomeEsco {

    String id;
    String esco;

    public LearningOutcomeEsco(String id, URL url) {
        this.id = id;
        this.esco = url.toString();
    }
}
