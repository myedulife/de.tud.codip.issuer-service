package rest;

import javax.enterprise.context.RequestScoped;
import javax.inject.Inject;
import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.MultivaluedMap;
import javax.ws.rs.core.Response;

import org.apache.commons.io.IOUtils;
import org.eclipse.microprofile.config.inject.ConfigProperty;
import org.jboss.logging.Logger;
import org.jboss.resteasy.plugins.providers.multipart.InputPart;
import org.jboss.resteasy.plugins.providers.multipart.MultipartFormDataInput;
import io.quarkus.security.Authenticated;
import parser.CertificateBuilder;
import parser.CertificateParser;
import user.UserManager;

import java.io.*;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.*;

@Path("/certificate")
@Authenticated
@RequestScoped
public class CertificateResource {

    private static final int MIN = 1;
    private static final int MAX = 100000;
    private static final Logger LOG = Logger.getLogger(CertificateResource.class);

    @ConfigProperty(name = "upload.directory")
    String UPLOAD_FOLDER;

    @ConfigProperty(name = "keystore.directory")
    String KEYSTORE_FOLDER;

    @ConfigProperty(name = "vc.directory")
    String VC_FOLDER;

    @Inject
    UserManager userManager;

    @POST
    @Path("/upload")
    @Consumes(MediaType.MULTIPART_FORM_DATA)
    public Response uploadTemplate(MultipartFormDataInput input) throws IOException {
        Map<String, List<InputPart>> uploadForm = input.getFormDataMap();
        // Get file data to save
        List<InputPart> inputParts = uploadForm.get("file");
        if (inputParts != null) {
            for (InputPart inputPart : inputParts) {
                try {
                    MultivaluedMap<String, String> header = inputPart.getHeaders();
                    String fileName = getFileName(header);
                    // convert the uploaded file to inputstream
                    InputStream inputStream = inputPart.getBody(InputStream.class, null);
                    byte[] bytes = IOUtils.toByteArray(inputStream);
                    //String path = System.getProperty("user.home") + File.separator + "uploads";
                    Files.createDirectories(Paths.get(UPLOAD_FOLDER));
                    Files.createDirectories(Paths.get(VC_FOLDER));
                    File customDir = new File(UPLOAD_FOLDER);
                    int x = MIN + (int)(Math.random() * ((MAX - MIN) + 1));
                    String randomNumber = String.valueOf(x);
                    fileName = customDir.getCanonicalPath() + File.separator + fileName.trim() + "." + randomNumber;
                    File uploadedFile = new File(fileName);
                    if (!uploadedFile.exists()) {
                        writeFile(bytes, fileName);
                        // start certificate workflow
                        processTemplateFile(uploadedFile);
                        return Response.status(200).entity("{\"message\": \"Uploaded file name: " + fileName + "\"}").build();
                    } else {
                        LOG.error("Uploaded file exist");
                        return Response.status(500).entity("{\"message\": \"Uploaded file exist\"}").build();
                    }
                } catch (Exception e) {
                    LOG.error("Upload failed", e);
                }
            }
        } else {
            LOG.error("Attachment is empty");
            return Response.status(500).entity("{\"message\": \"Attachment is empty\"}").build();
        }
        LOG.error("Upload failed");
        return Response.status(500).entity("{\"message\": \"Upload failed\"}").build();
    }

    private void processTemplateFile(File uploadedFile) {
        Optional<CertificateParser> certificateParser = CertificateBuilder.getInstance().buildParser(uploadedFile);
        if (certificateParser.isEmpty()) {
            LOG.error("Cannot found compatible parser.");
        } else {
            certificateParser.get().parse(uploadedFile, userManager, KEYSTORE_FOLDER, VC_FOLDER);
        }

    }

    private String getFileName(MultivaluedMap<String, String> header) {
        String[] contentDisposition = header.getFirst("Content-Disposition").split(";");
        for (String filename : contentDisposition) {
            if ((filename.trim().startsWith("filename"))) {
                String[] name = filename.split("=");
                String finalFileName = name[1].trim().replaceAll("\"", "");
                return finalFileName;
            }
        }
        return "unknown";
    }
    // Utility method
    private void writeFile(byte[] content, String filename) throws IOException {
        File file = new File(filename);
        if (!file.exists()) {
            file.createNewFile();
        }
        FileOutputStream fop = new FileOutputStream(file);
        fop.write(content);
        fop.flush();
        fop.close();
    }

}
