package rest;

import org.eclipse.microprofile.config.inject.ConfigProperty;

import javax.ws.rs.GET;
import javax.ws.rs.Path;

@Path("/")
public class GreetingResource {

    @ConfigProperty(name = "quarkus.application.version")
    String version;

    @GET
    public String hello() {
        return "<html><head><title>Hello</title></head><body><h1>Hello, here is the backend in version " + version + ".</h1></body></html>";
    }

}
