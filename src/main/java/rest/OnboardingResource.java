package rest;

import javax.enterprise.context.RequestScoped;
import javax.inject.Inject;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.core.Response;

import org.jboss.logging.Logger;
import rest.model.Register;
import user.UserManager;

import java.io.IOException;
import java.util.Optional;
import java.util.regex.Pattern;

@RequestScoped
@Path("/onboarding")
public class OnboardingResource {

    private static final Logger LOG = Logger.getLogger(OnboardingResource.class);

    @Inject
    UserManager userManager;

    @GET
    @Path("/hello")
    public String hello() {
        return "Hello";
    }

    @POST
    @Path("/signup")
    public Response signup(Register register) throws IOException {
        String nickname = register.getNickname();
        String email = register.getEmail();
        String firstName = register.getFirstName();
        String lastName = register.getLastName();
        String password = register.getPassword();
        if (nickname != null && email != null && password != null && patternMatches(email)) {
            return createUser(nickname, email, firstName, lastName, password);
        } else {
            LOG.error("Missing or wrong data");
            return Response.status(500).entity("{\"message\": \"Missing or wrong data\"}").build();
        }
    }

    private boolean patternMatches(String emailAddress) {
        return Pattern.compile("^[a-zA-Z0-9_!#$%&'*+/=?`{|}~^.-]+@[a-zA-Z0-9.-]+$")
                .matcher(emailAddress)
                .matches();
    }

    private Response createUser(String nickname, String email, String firstName, String lastName, String password) {
        LOG.info("Create user");
        Optional<Response> response = userManager.createUser(nickname, email, firstName, lastName, password);
        if (response.isEmpty()) {
            LOG.warn("User exist");
            return Response.status(500).entity("{\"message\": \"User exist\"}").build();
        } else {
            Response.StatusType info = response.get().getStatusInfo();
            if (!"Created".equals(info.getReasonPhrase())) {
                LOG.warn(info.getReasonPhrase() + " Code: " + Integer.toString(info.getStatusCode()));
                return Response.status(500).entity("{\"message\": \"User was not registered. Reason: " + info.getReasonPhrase() + " Code: " + Integer.toString(info.getStatusCode()) + "\"}").build();
            }
            return Response.status(200).entity("{\"message\": \"User registered now\"}").build();
        }
    }
}
