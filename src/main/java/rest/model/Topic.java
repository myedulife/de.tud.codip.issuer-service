package rest.model;

import lombok.Getter;
import lombok.Setter;

import java.util.ArrayList;
import java.util.List;

@Getter
@Setter
public class Topic {
    private String title;
    private String mark;
    private int hours;
    private List<String> learningOutcomes = new ArrayList<>();
    private List<EscoSkill> escoSkills = new ArrayList<>();
}
