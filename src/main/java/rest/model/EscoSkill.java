package rest.model;

import lombok.Getter;
import lombok.Setter;

import java.net.URL;

@Getter
@Setter
public class EscoSkill {
    private URL skill;

    public EscoSkill(URL url) {
        this.skill = url;
    }
}
