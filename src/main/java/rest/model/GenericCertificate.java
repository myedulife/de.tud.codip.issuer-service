package rest.model;

import lombok.Getter;
import lombok.Setter;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

@Getter
@Setter
public class GenericCertificate {

    private String id = "urn:myedulife:training:1";
    private String gender = "";
    private String familyName = "";
    private String givenName = "";
    private String birthday = "";
    private LocalDate start;
    private LocalDate end;
    private String courseName = "";
    private String courseDescription = "";
    private String place = "";
    private int hours;
    private String issuanceDate = "";
    private String certificateNumber;
    private String managingDirector;
    private String seminarLeader;
    private List<Topic> topics = new ArrayList<>();

    public GenericCertificate() {

    }

    @Override
    public String toString() {
        // topics StringBuilder
        StringBuilder sbTopics = new StringBuilder();
        sbTopics.append("[");
        for (Topic topic : this.topics) {
            if (topic == null) {
                sbTopics.append("null");
            } else {
                sbTopics.append("(");
                sbTopics.append(topic.getTitle());
                sbTopics.append(",");
                sbTopics.append(topic.getHours());
                sbTopics.append(",");
                sbTopics.append(topic.getMark());
                sbTopics.append(")");
            }
            sbTopics.append("|");
        }
        sbTopics.append("]");
        // start and end localdate stringbuilders
        StringBuilder sbStart = new StringBuilder();
        if (this.start != null) sbStart.append(this.start.toString());
        StringBuilder sbEnd = new StringBuilder();
        if (this.end != null) sbEnd.append(this.end.toString());

        StringBuilder sb = new StringBuilder()
                .append("Gender: ").append(this.gender)
                .append(", family name: ").append(this.familyName)
                .append(", given name: ").append(this.givenName)
                .append(", Birthday: ").append(this.birthday)
                .append(", Start: ").append(sbStart.toString())
                .append(", End: ").append(sbEnd.toString())
                .append(", Coursename: ").append(this.courseName)
                .append(", Place: ").append(this.place)
                .append(", IssuanceDate: ").append(this.issuanceDate)
                .append(", Certificate Number: ").append(this.certificateNumber)
                .append(", Hours: ").append(Integer.toString(this.hours))
                .append(", Managing Director: ").append(this.managingDirector)
                .append(", Seminar Leader: ").append(this.seminarLeader)
                .append(", Topics: ").append(sbTopics.toString());
        return sb.toString();
    }
}
