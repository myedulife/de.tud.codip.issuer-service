package rest.model;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class Register {
    private String nickname;
    private String email;
    private String firstName;
    private String lastName;
    private String password;

    public Register() {
    }

    public Register(String nickname, String email) {
        this.nickname = nickname;
        this.email = email;
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder()
                .append("Nickname: ").append(this.nickname)
                .append(", family name: ").append(this.lastName)
                .append(", given name: ").append(this.firstName)
                .append(", Email: ").append(this.email)
                .append(", Password: ").append(this.password);
        return sb.toString();
    }
}
